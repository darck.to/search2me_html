<!doctype html>
<html class="no-js has-navbar-fixed-top" lang="">

<head>
  <meta charset="utf-8">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="manifest" href="site.webmanifest">
  <link rel="apple-touch-icon" href="icon.png">
  <!-- Place favicon.ico in the root directory -->

  <link rel="stylesheet" href="css/bulma.css">
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/main.css">
  <link type="text/css" rel="stylesheet" href="css/all.min.css"  media="screen,projection"/>
  <link rel="stylesheet" type="text/css" href="css/leaflet.css">
  <link rel="stylesheet" href="css/esri-leaflet-geocoder.css">
  <link rel="stylesheet" href="css/leaflet-routing-machine.css" />

  <meta name="theme-color" content="#fafafa">
  <title>search2me Busquedas</title>
</head>

<body>
  <!--[if IE]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
  <![endif]-->

  <!--Search Navigator-->
  <nav id="navigator" class="navbar is-fixed-top has-shadow" role="navigation" aria-label="main navigation">
  </nav>
  <!--Filtro Buscar-->
  <div id="buscar-filter">
  </div>

  <div class="columns is-multiline has-background-white-ter he-100v ove-scroll ma-no-b">
    <!--Results Buscar-->
    <div class="column is-6 pa-one he-100p ove-scroll">
      <section>
        <h1 class="has-text-left fo-w-l ma-no-t pa-lr-one">Resultados</h1>
        <div id="buscar-results" class="columns is-multiline">
        </div>
      </section>
    </div>
    <!--Map Buscar-->
    <div id="buscar-map" class="column is-6 has-background-grey-light he-100p ove-scroll">
    </div>
  </div>

  <!--Footer-->
  <div id="footer">
  </div>

  <script src="js/vendor/modernizr-3.8.0.min.js"></script>
  <script type="text/javascript" src="js/leaflet/leaflet.js"></script>
  <script type="text/javascript" src="js/leaflet/esri-leaflet.js"></script>
  <script type="text/javascript" src="js/leaflet/esri-leaflet-geocoder.js"></script>
  <script type="text/javascript" src="js/leaflet/leaflet-routing-machine.js"></script>
  <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.4.1.min.js"><\/script>')</script>
  <script src="js/plugins.js"></script>
  <script src="js/lang.js"></script>
  <script src="js/buscar/buscar.js"></script>
</body>

</html>

<script type="text/javascript">
//ELEMENTOS DE BUSQUEDA
  <?php
    $renta = 4;
    $tipo = 0;
    $clave = 0;
    if (!empty($_GET)) {
      if ($_GET['renta'] == 0) {
        $renta = $_GET['renta'];
      }
      if ($_GET['renta'] == 1) {
        $renta = $_GET['renta'];
      }
      if ($_GET['renta'] == 3) {
        $renta = $_GET['renta'];
      }
      if (!empty($_GET['cate_index'])) {
        $clave = $_GET['cate_index'];
        $tipo = 1;
      }
      if (!empty($_GET['perf_index'])) {
        $clave = $_GET['perf_index'];
        $tipo = 2;
      }
    }
    echo "carga_busqueda_renta(".$renta.", ".$tipo.", '".$clave."');";
  ?>
</script>
