<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');
  include_once('../../functions/functions.php');

  if (empty($_POST['nom']) || empty($_POST['pas'])) {
    echo "El usuario o la contraseña no han sido ingresados correctamente!";
  } else {
    // "limpiamos" los campos del formulario de posibles códigos maliciosos
    $usuario_nombre = mysqli_real_escape_string($mysqli,$_POST['nom']);
    $usuario_clave = mysqli_real_escape_string($mysqli,$_POST['pas']);
    $usuario_clave = md5($usuario_clave);

    // comprobamos que los datos ingresados en el formulario coincidan con los de la BD
    $sqlogin = $mysqli->query("SELECT auth_number, nom FROM init_auth WHERE nom = '".$usuario_nombre."' AND pas = '".$usuario_clave."'");
    if ($sqlogin->num_rows > 0) {
      $row = $sqlogin->fetch_assoc();
      $auth_nombre = $row["nom"];
      $auth_number = $row['auth_number'];
      $resultados[] = array("success"=> true, "aUth_key"=> $auth_number, "aUth_user"=> $auth_nombre);
    } else {
      $resultados[] = array("success"=> false, "error"=> "Error, contact support");
      //$resultados[] = array("success"=> false, "error"=> mysqli_error($mysqli));
    }
  }

  print json_encode($resultados);
  // incluimos el archivo de desconexion a la Base de Datos
  include('../../functions/cierra_conexion.php');
?>
