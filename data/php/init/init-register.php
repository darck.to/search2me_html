<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');
  include_once('../../functions/functions.php');

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');

  // "limpiamos" los campos del formulario de posibles códigos maliciosos
  $usuario_nombre = mysqli_real_escape_string($mysqli, $_POST['nom']);
  $usuario_email = mysqli_real_escape_string($mysqli, $_POST['mai']);
  $usuario_clave = mysqli_real_escape_string($mysqli, $_POST['pas']);
  $usuario_plan = mysqli_real_escape_string($mysqli, $_POST['pla']);

  $auth_number = generateRandomString(18);
  $init_index = generateRandomString(12);

  // comprobamos que el usuario ingresado no haya sido registrado antes
  $sql = $mysqli->query("SELECT nom FROM init_auth WHERE nom ='".$usuario_nombre."' OR mai ='".$usuario_email."'");
  if ($sql->num_rows > 0) {
    $resultados[] = array("success"=> false, "error"=> "el usuario y/o correo ya han sido registrado previamente");
  } else {
    $usuario_clave = md5($usuario_clave); // encriptamos la contraseña ingresada con md5

    // ingresamos los datos a la BD TLABLA INDEX
    $sqlReg = $mysqli->query("INSERT INTO init_auth (nom, pas, mai, auth_number, init_index) VALUES ('".$usuario_nombre."', '".$usuario_clave."', '".$usuario_email."', '".$auth_number."', '".$init_index."')");
    if($sqlReg) {
      $resultados[] = array("success"=> true, "aUth_key"=> $auth_number, "aUth_user"=> $usuario_nombre);
    } else {
      $resultados[] = array("success"=> false, "error"=> "Error, contact support");
      //$resultados[] = array("success"=> false, "error"=> mysqli_error($mysqli));
    }
    //GUARDAMOS EL PERFIL
    $sqlPerf = $mysqli->query("INSERT INTO perf_br (perf_index, plus_index) VALUES ('".$init_index."', '".$usuario_plan."')");

  }

  print json_encode($resultados);
  // incluimos el archivo de desconexion a la Base de Datos
  include('../../functions/cierra_conexion.php');
?>
