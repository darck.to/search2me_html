<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');

  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);
  $num = mysqli_real_escape_string($mysqli,$_POST['num']);
  $num = ($num-1);

  $sql_auth =  $mysqli->query("SELECT init_index, nom FROM init_auth WHERE auth_number = '".$auth."' AND nom = '".$user."' ");
  if ($sql_auth->num_rows > 0) {
    $row = $sql_auth->fetch_assoc();
    //NOMBRE DE ARCHIVO PARA LEER LOS FILTROS
    $filename = '../../assets/filt_br/'.$row['init_index'].'/'.$row['init_index'].'_' . $num . '.json';
    if (file_exists($filename)) {
      unlink($filename);
      $resultados[] = array("success"=>true, "error"=>"Filtro borrado");
    } else {
      $resultados[] = array("success"=>false, "error"=>'Error, consulta soporte '.$filename);
    }
    print json_encode($resultados);
  } else {
    print json_encode('Error');
  }

  include('../../functions/cierra_conexion.php');
?>
