<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');

  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);

  $sql_auth =  $mysqli->query("SELECT init_index, nom FROM init_auth WHERE auth_number = '".$auth."' AND nom = '".$user."' ");
  if ($sql_auth->num_rows > 0) {
    $row = $sql_auth->fetch_assoc();

    $sql_perf = $mysqli->query("SELECT `plus_index` FROM `perf_br` WHERE `perf_index` = '".$row['init_index']."'");
    if ($sql_perf->num_rows > 0) {
      $row_perf = $sql_perf->fetch_assoc();
      $filename = '../../assets/plus_br/paq_' . $row_perf['plus_index'] . '.json';
      $filename = file_get_contents($filename);
			$json = json_decode($filename, true);
      foreach ($json as $content) {
        $filter = $content['filt'];
      }
    }

    //NOMBRE DE ARCHIVO PARA LEER LOS FILTROS
    for ($i=0; $i < $filter; $i++) {
      $filename = '../../assets/filt_br/'.$row['init_index'].'/'.$row['init_index'].'_' . $i . '.json';
      if (file_exists($filename)) {
        $filename = file_get_contents($filename);
  			$json = json_decode($filename, true);
        foreach ($json as $content) {
          $resultados[] = array("success"=>true, "renta"=>$content['renta'], "categorias"=>$content['categorias'], "precio"=>$content['precio'], 'cuartos'=>$content['cuartos'], 'jacuzzi'=>$content['jacuzzi'], 'air'=>$content['air'], "jardin"=>$content['jardin'], "chimenea"=>$content['chimenea'], "banos"=>$content['banos'], "cochera"=>$content['cochera'], "piscina"=>$content['piscina'], "terraza"=>$content['terraza'], "balcon"=>$content['balcon'], "seguridad"=>$content['seguridad'], "recepcion"=>$content['recepcion'], "gimnasio"=>$content['gimnasio'], "mail"=>$content['mail'], "num"=>$i+1);
        }
      } else {
        $resultados[] = array("success"=>false, "error"=>'Error, consulta soporte');
      }
    }

    print json_encode($resultados);

  } else {
    print json_encode('Error');
  }

  include('../../functions/cierra_conexion.php');
?>
