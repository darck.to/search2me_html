<?php
  // SDK de Mercado Pago
  require '../../vendor/autoload.php';

  // Agrega credenciales
  MercadoPago\SDK::setAccessToken('APP_USR-8650268560556436-120306-871b67254c5c9cd046b3566e2cb281b1-496317876');

  // Crea un objeto de preferencia
  $preference = new MercadoPago\Preference();

  // Crea un ítem en la preferencia
  $item = new MercadoPago\Item();
  $item->title = 'Pago de Plan Plus Bienes Raices Tijuana';
  $item->quantity = 1;
  $item->unit_price = $_POST['coin'];
  $item->currency_id = $_POST['symbol'];
  $preference->items = array($item);
  $preference->save();

  echo $preference->init_point;
?>
