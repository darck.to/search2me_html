<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');
  include_once('../../functions/functions.php');

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');

  // "limpiamos" los campos del formulario de posibles códigos maliciosos
  $tit = mysqli_real_escape_string($mysqli, $_POST['tit']);
  $lat = mysqli_real_escape_string($mysqli, $_POST['lat']);
  $lng = mysqli_real_escape_string($mysqli, $_POST['lng']);
  $cal = mysqli_real_escape_string($mysqli, $_POST['cal']);
  $num = mysqli_real_escape_string($mysqli, $_POST['num']);
  $col = mysqli_real_escape_string($mysqli, $_POST['col']);
  $ciu = mysqli_real_escape_string($mysqli, $_POST['ciu']);
  $est = mysqli_real_escape_string($mysqli, $_POST['est']);
  $pai = mysqli_real_escape_string($mysqli, $_POST['pai']);
  $cp = mysqli_real_escape_string($mysqli, $_POST['cp']);
  $des = mysqli_real_escape_string($mysqli, $_POST['des']);
  $ren = mysqli_real_escape_string($mysqli, $_POST['ren']);
  $cur = mysqli_real_escape_string($mysqli, $_POST['cur']);
  $pre = mysqli_real_escape_string($mysqli, $_POST['pre']);
  $cate_index = mysqli_real_escape_string($mysqli, $_POST['cat']);
  $cuartos = mysqli_real_escape_string($mysqli, $_POST['cuartos']);
  $jacuzzi = mysqli_real_escape_string($mysqli, $_POST['jacuzzi']);
  $air = mysqli_real_escape_string($mysqli, $_POST['air']);
  $jardin = mysqli_real_escape_string($mysqli, $_POST['jardin']);
  $trasero = mysqli_real_escape_string($mysqli, $_POST['trasero']);
  $chimenea = mysqli_real_escape_string($mysqli, $_POST['chimenea']);
  $banos = mysqli_real_escape_string($mysqli, $_POST['banos']);
  $cochera = mysqli_real_escape_string($mysqli, $_POST['cochera']);
  $piscina = mysqli_real_escape_string($mysqli, $_POST['piscina']);
  $terraza = mysqli_real_escape_string($mysqli, $_POST['terraza']);
  $balcon = mysqli_real_escape_string($mysqli, $_POST['balcon']);
  $seguridad = mysqli_real_escape_string($mysqli, $_POST['seguridad']);
  $recepcion = mysqli_real_escape_string($mysqli, $_POST['recepcion']);
  $gimnasio = mysqli_real_escape_string($mysqli, $_POST['gimnasio']);
  $mult = mysqli_real_escape_string($mysqli, $_POST['mult']);

  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);

  $pro_index = generateRandomString(12);

  $sql =  $mysqli->query("SELECT init_index, nom FROM init_auth WHERE auth_number = '".$auth."' AND nom = '".$user."' ");
  if ($sql->num_rows > 0) {
    $row = $sql->fetch_assoc();
    $init_index = $row['init_index'];
    //ingresamos los datos a la BDD PRO_BR
    $sqlPro = $mysqli->query("INSERT INTO pro_br (pro_index, nom, lat, lng, ren, pre, cur, cate_index, perf_index) VALUES ('".$pro_index."', '".$tit."', '".$lat."', '".$lng."', '".$ren."', '".$pre."', '".$cur."', '".$cate_index."', '".$init_index."')");
    if($sqlPro) {
      //ingresamos los datos a la BDD DIRE_BR
      $sqlDire = $mysqli->query("INSERT INTO dire_br (pro_index, cal, num, col, ciu, cp, est, pai) VALUES ('".$pro_index."', '".$cal."', '".$num."', '".$col."', '".$ciu."', '".$cp."', '".$est."', '".$pai."')");
      if($sqlDire) {
        //ingresamos los datos a la BDD DESC_BR
        $sqlDesc = $mysqli->query("INSERT INTO desc_br (pro_index, des) VALUES ('".$pro_index."', '".$des."')");
        if($sqlDesc) {
          //ingresamos los datos al JSON categorias
          $cabecera[] = array('pro_index'=> $pro_index, 'cuartos'=>$cuartos, 'jacuzzi'=>$jacuzzi, 'air'=>$air, 'jardin'=> $jardin, 'trasero'=> $trasero, 'chimenea'=> $chimenea, 'banos'=> $banos, 'cochera'=> $cochera, 'piscina'=> $piscina, 'terraza'=> $terraza, 'balcon'=> $balcon, 'seguridad'=> $seguridad, 'recepcion'=> $recepcion, 'gimnasio'=> $gimnasio, "multimedia"=>$mult);
        	//NOMBRE DE ARCHIVO PARA GUARDAR LAS OPC
        	$fileName = '../../assets/opc_br/'.$pro_index.'_opc.json';
        	if (file_exists($fileName)) {
        	} else {
        		$fileFinal = fopen($fileName, 'w') or die ('No se guardo el archivo \n');
        		fwrite($fileFinal, json_encode($cabecera, JSON_PRETTY_PRINT));
        		fclose($fileFinal);
            //GUARDAMOS LAS IMAGENES DEL PRODUCTO EN SU DESTINO FINAL, DE EXISTIR
            $carpeta = "../../assets/pro_img/".$pro_index;
            if (!is_dir($carpeta)) {
              //CREAMOS LA CARPETA DESTINO
              mkdir($carpeta, 0755, true);
            }
            $fileList = glob('../../assets/pro_img/pre/' . $init_index . "_*.png");
          	//RECORREMOS LOS ARCHIVOS
            $n = 0;
          	foreach($fileList as $filename){
              if ( file_exists($carpeta."/".$pro_index."_" . $n . ".png") ) {
                unlink($carpeta."/".$pro_index."_" . $n . ".png");
              }
              rename($filename, $carpeta."/".$pro_index."_" . $n . ".png");
              $n++;
          	}
            $resultados[] = array("success"=> true, "info"=>"Producto ingresado");
        	}
        } else {
          $resultados[] = array("success"=> false, "error"=> "Error en ingreso de descripcion, contact support");
        }
      } else {
        $resultados[] = array("success"=> false, "error"=> "Error en ingreso de direccion, contact support");
      }
    } else {
      $resultados[] = array("success"=> false, "error"=> "Error general, contact support");
      //$resultados[] = array("success"=> false, "error"=> mysqli_error($mysqli));
    }
  } else {
    $resultados[] = array("success"=> false);
  }

  print json_encode($resultados);
  // incluimos el archivo de desconexion a la Base de Datos
  include('../../functions/cierra_conexion.php');
?>
