<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');

  $fileoptions = file_get_contents('../../assets/opc_br/opc_br.json');
  $data = json_decode($fileoptions, true);

  print json_encode($data);
?>
