<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');

  $sqlpai =  $mysqli->query("SELECT count(pai) as num, pai FROM dire_br GROUP BY pai");
  if ($sqlpai->num_rows > 0) {
    while ($rowpai = $sqlpai->fetch_assoc() ) {
      $resultados[] = array("success"=>true, "pai"=>$rowpai['pai'], "num"=>$rowpai['num']);
    }
  } else {
    $resultados[] = array("success"=>false, "error"=>"There was an error, please contact support");
  }

  print json_encode($resultados);

  include('../../functions/cierra_conexion.php');
?>
