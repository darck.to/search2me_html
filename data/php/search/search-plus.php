<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');

  //PERFILES PLUS
  $sql_auth =  $mysqli->query("SELECT perf_index FROM perf_br WHERE plus_index = 2");
  if ($sql_auth->num_rows > 0) {
    while ($row_auth = $sql_auth->fetch_assoc()) {
      //CONSULTA DE PROPIEDADES PLUS
      $sql = $mysqli->query("SELECT pro_index, cate_index, nom, pre, cur, ren FROM pro_br WHERE perf_index = '".$row_auth['perf_index']."' ORDER BY RAND() LIMIT 5");
      if ($sql->num_rows > 0) {
        while ($row = $sql->fetch_assoc()) {
          //LEEMOS LA CATEGORIA
          $sqlcategoria = $mysqli->query("SELECT nom FROM cate_br WHERE cate_index = '".$row['cate_index']."'");
          if ($sqlcategoria->num_rows > 0) {
            $rowcategoria = $sqlcategoria->fetch_assoc();
            $categoria = $rowcategoria['nom'];
          }
          //LEEMOS DIRECCION
          $sql_dir =  $mysqli->query("SELECT col, ciu, est, pai FROM dire_br WHERE pro_index = '".$row['pro_index']."'");
          if ($sql_dir->num_rows > 0) {
            $row_dir = $sql_dir->fetch_assoc();
            //LEE JSON CONFIG
            $filename = file_get_contents('../../assets/opc_br/'.$row['pro_index'].'_opc.json');
            $data = json_decode($filename, true);
            //RESULTADOS
            $resultados[] = array("success"=>true, 'pro_index'=>$row['pro_index'], 'nom'=>$row['nom'], "ren"=>$row['ren'], "pre"=>$row['pre'], "cur"=>$row['cur'], "cat"=>$categoria, "col"=>$row_dir['col'], "ciu"=>$row_dir['ciu'], "est"=>$row_dir['est'], "pai"=>$row_dir['pai'], 'cuartos'=>$data[0]['cuartos'], 'jacuzzi'=>$data[0]['jacuzzi'], 'air'=>$data[0]['air'], "jardin"=>$data[0]['jardin'], "trasero"=>$data[0]['trasero'], "chimenea"=>$data[0]['chimenea'], "banos"=>$data[0]['banos'], "cochera"=>$data[0]['cochera'], "piscina"=>$data[0]['piscina'], "terraza"=>$data[0]['terraza'], "balcon"=>$data[0]['balcon'], "seguridad"=>$data[0]['seguridad']);
          } else{
            $resultados[] = array("success"=>false, "error"=>'Error, en direccion');
          }
        }
      }
    }
  } else {
    $resultados[] = array("success"=>false, "error"=>'Error, no resultados');
  }
  print json_encode($resultados);

  include('../../functions/cierra_conexion.php');
?>
