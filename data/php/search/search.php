<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');

  if (!empty($_POST)) {
    $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
    $user = mysqli_real_escape_string($mysqli,$_POST['user']);
    $authorized_query = " WHERE auth_number = '".$auth."' AND nom = '".$user."'";
  } else {
    $authorized_query = "";
  }

  $is_where = "";
  $is_and = "";
  $consulta_lim = "";
  $consulta_cate = "";
  $is_renta = "";
  $consulta_pre = "";
  $consulta_final = "";
  $is_pai = ""; $is_ciu = "";
  //VIENE DE CATEGORIAS
  if ($_POST['cate_index'] == "null") {
    $consulta_lim = "LIMIT 50";
    $consulta_final = "SELECT pro_index, nom FROM pro_br $consulta_lim";
  } else {
    $cate_index = mysqli_real_escape_string($mysqli,$_POST['cate_index']);
    $consulta_categorias = "WHERE cate_index = '".$cate_index."' LIMIT 50";
    $consulta_final = "SELECT pro_index, nom FROM pro_br $consulta_categorias";
  }
  //VIENE DE FILTROS
  if ( $_POST['complex'] == "yes" ) {
    if (strlen($_POST['categorias']) < 12) {
      $is_where = " AND ";
    }
    if (strlen($_POST['categorias']) == 12) {
      $consulta_cate = " AND `pro_br`.`cate_index` = '" . $_POST['categorias'] . "' AND ";
    }
    $is_renta = " AND `ren` = '" . $_POST['renta'] . "' ";
    $desde = $_POST['desde'];
    $hasta = $_POST['hasta'];
    $consulta_pre = $is_where." `pro_br`.`pre` BETWEEN " . $desde . " AND " . $hasta;
    $consulta_cate_pre = $consulta_cate . '' . $consulta_pre;
    //PAISES
    $paises = $_POST['paises'];
    $ciudades = $_POST['ciudades'];
    if ($paises <> "0") {
      $is_pai = " AND `dire_br`.`pai` = '".$paises. "'";
      if ($ciudades <> "0") {
        $is_ciu = " AND `dire_br`.`ciu` = '" . $ciudades . "'";
      }
    }
    $consulta_final = "SELECT `pro_br`.`pro_index`, `pro_br`.`nom` FROM `pro_br` INNER JOIN `dire_br` ON `pro_br`.`pro_index`=`dire_br`.`pro_index`" . $consulta_cate_pre . $is_pai . $is_ciu . $is_renta;
  } else {
  }

  $sql_auth =  $mysqli->query("SELECT init_index FROM init_auth" . $authorized_query);
  if ($sql_auth->num_rows > 0) {
    $row = $sql_auth->fetch_assoc();
    //CONSULTA CATEGORIAS Y PRECIO
    $sqlPro =  $mysqli->query($consulta_final);
    if ($sqlPro->num_rows > 0) {
      while ($rowPro = $sqlPro->fetch_assoc()) {
        $pro_index[] = $rowPro['pro_index'];
        $pro_id = $rowPro['pro_index'];
        $nombre = $rowPro['nom'];
        //LIMPIAMOS EL ARRAY DE DUPLICADOS Y LEEMOS EL RESULTADO PARA IMPRESION
        $pro_index = array_unique($pro_index, SORT_REGULAR);
        foreach($pro_index as $content) {
          if ($content == $pro_id) {

            //LEEMOS LAS OPCIONES DE LAS PUBLICACIONES PARA IDENTIFICAR LAS ENVIADAS POR EL FILTRO DE BUSQUEDA
            if ( $_POST['opciones'] == "true") {
              $fileoptions = file_get_contents('../../assets/opc_br/'.$pro_id.'_opc.json');
              $data = json_decode($fileoptions, true);
              //LEE JSON CONFIG
              $a = array($data[0]['cuartos'], $data[0]['jacuzzi'], $data[0]['air'], $data[0]['jardin'], $data[0]['chimenea'], $data[0]['banos'], $data[0]['cochera'], $data[0]['piscina'], $data[0]['terraza'], $data[0]['balcon'], $data[0]['seguridad'], $data[0]['recepcion'], $data[0]['gimnasio']);
              $b = array($cua = $_POST['cuartos'], $jac = $_POST['jacuzzi'], $air = ['air'], $jar = $_POST['jardin'], $chi = $_POST['chimenea'], $ban = $_POST['banos'], $coc = $_POST['cochera'], $pis = $_POST['piscina'], $ter = $_POST['terraza'], $bal = $_POST['balcon'], $seg = $_POST['seguridad'], $rec = $_POST['recepcion'], $seg = $_POST['gimnasio']);
              $number = array_intersect($a,$b);
              if (count($number) > 0) {
                $resultados[] = array("success"=>true,'pro_index'=>$pro_id, 'nom'=>$nombre, "cuartos"=>$data[0]['cuartos'], "jacuzzi"=>$data[0]['jacuzzi'], "air"=>$data[0]['air'], "jardin"=>$data[0]['jardin'], "trasero"=>$data[0]['trasero'], "chimenea"=>$data[0]['chimenea'], "banos"=>$data[0]['banos'], "cochera"=>$data[0]['cochera'], "piscina"=>$data[0]['piscina'], "terraza"=>$data[0]['terraza'], "balcon"=>$data[0]['balcon'], "seguridad"=>$data[0]['seguridad'], "recepcion"=>$data[0]['recepcion'], "gimnasio"=>$data[0]['gimnasio']);
              }
            } else {
              $filename = file_get_contents('../../assets/opc_br/'.$pro_id.'_opc.json');
              $json = json_decode($filename, true);
              $resultados[] = array("success"=>true,'pro_index'=>$pro_id, 'nom'=>$nombre, "cuartos"=>$json[0]['cuartos'], "jacuzzi"=>$json[0]['jacuzzi'], "air"=>$json[0]['air'], "jardin"=>$json[0]['jardin'], "trasero"=>$json[0]['trasero'], "chimenea"=>$json[0]['chimenea'], "banos"=>$json[0]['banos'], "cochera"=>$json[0]['cochera'], "piscina"=>$json[0]['piscina'], "terraza"=>$json[0]['terraza'], "balcon"=>$json[0]['balcon'], "seguridad"=>$json[0]['seguridad'], "recepcion"=>$json[0]['recepcion'], "gimnasio"=>$json[0]['gimnasio']);
            }

          }
        }
      }
    } else {
      $resultados[] = array("success"=>false, "error"=>'Error, no resultados ' . $consulta_ciudad);
    }

    print json_encode($resultados);

  } else {
    print json_encode('Error');
  }

  include('../../functions/cierra_conexion.php');
?>
