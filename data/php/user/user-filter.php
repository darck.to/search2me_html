<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');

  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);
  $id = mysqli_real_escape_string($mysqli,$_POST['id']);

  $sql_auth =  $mysqli->query("SELECT init_index FROM init_auth WHERE auth_number = '".$auth."' AND nom = '".$user."' ");
  if ($sql_auth->num_rows > 0) {
    $row = $sql_auth->fetch_assoc();
    $init_index = $row['init_index'];
    $fileoptions = file_get_contents('../../assets/filt_br/' . $init_index . '/' . $init_index . '_' . $id . '.json');
    $data = json_decode($fileoptions, true);
  }

  print json_encode($data);

  include('../../functions/cierra_conexion.php');
?>
