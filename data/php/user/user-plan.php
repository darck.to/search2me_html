<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../../functions/abre_conexion.php');

  $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
  $user = mysqli_real_escape_string($mysqli,$_POST['user']);

  $sql_auth =  $mysqli->query("SELECT init_index FROM init_auth WHERE auth_number = '".$auth."' AND nom = '".$user."' ");
  if ($sql_auth->num_rows > 0) {
    $row = $sql_auth->fetch_assoc();
    $init_index = $row['init_index'];

    $sql_perf = $mysqli->query("SELECT `plus_index` FROM `perf_br` WHERE `perf_index` = '".$init_index."'");
    if ($sql_perf->num_rows > 0) {
      $row_perf = $sql_perf->fetch_assoc();
      //PLANES INDEX
      $plan = $row_perf['plus_index'];
      //PLANES FILES
      $filename = '../../assets/plus_br/paq_' . $plan . '.json';
      if (file_exists($filename)) {
        $filename = file_get_contents($filename);
        $json = json_decode($filename, true);
        foreach ($json as $content) {
          $resultados[] = array('success'=>true, 'plan'=>$plan, 'price'=>$content['price'], 'plus_index'=>$content['plus_index'], 'opc'=>$content['opc'], 'mult'=>$content['mult'], 'filt'=>$content['filt'], 'pub'=>$content['pub'], 'exp'=>$content['exp'], 'user_plan'=>$planPerf);
        }
      }
    } else {
      $resultados[] = array('success'=>false, 'error'=>'Error, contacta soporte');
    }

    print json_encode($resultados);

  } else {
    print json_encode('Error');
  }

  include('../../functions/cierra_conexion.php');
?>
