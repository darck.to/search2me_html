$(document).ready(function(){
  //CASCADE LOADING search loader --- opciones
  carga_search_slider();
  //CASCADA LOADING opciones
  carga_search_cat()

})
//COMIENZA A RECIBIR DATOS DE LOS INPUTS
$('.search-string').on('keyup', function(e) {
  if (this.value.length > 3) {
    carga_busqueda_pre()
  }
})
$('.search-price').on('change', function(e) {
  $('#inputPrice').val($('#slider1').val());
  var value = $('.search-option');
  var values = []
  value.each(function(){
    values.push(this.value)
  });
  if (configura_busqueda.called) {
  } else {
    configura_busqueda(values)
  }
})
$('#slider1').on('change paste keyup', function() {
  if (this.value.length > 3) {
    carga_busqueda_pre()
  }
  $('.input-price-container').removeClass('is-hidden')
})
$('#inputPrice').focusout(function() {
  $('#slider1').val($(this).val());
  $('#number1').html(money_format(this.value));
  setTimeout($('.input-price-container').addClass('is-hidden'), 2000)
})
$('#inputPrice').on('keypress',function(e) {
  if(e.which == 13) {
    $('#number1').html(money_format(this.value));
    $('#slider1').val($(this).val());
    carga_busqueda_pre();
    $('.input-price-container').addClass('is-hidden')
  }
});
//CARGA BUSQUEDA PRE (TODOS LOS PRICE Y TXT)
function carga_busqueda_pre(e){
  var value = $('.search-option');
  var values = [];
  value.each(function(){
    values.push(this.value);
  });
  if (configura_busqueda.called) {
  } else {
    configura_busqueda(values)
  }
}
//RENTA COMPARA
$('.search-ren').on('change', function(e) {
  var searchs = [];
  $.each($("input[name='ren']:checked"), function(){
    searchs.push($(this).val());
  })
  var result = $('.search-container').map(function() {
    var row =[];
    row.push($(this).find('input[name="tag-ren"]').val());
    return [row]
  }).get();
  $('.published').addClass('no-block')
  for (var i = 0; i < result.length; i++) {
    $.each(result[i], function(index, value) {
      $.each(searchs, function(id, content) {
        if (index == id) {
          if (value == content) {
            console.log(value, content);
            $('.publish-' + i).removeClass('no-block')
          }
        }
      })
    })
  }
})
//CATEGORIA COMPARA
$('#select-cate').on('change', function(e) {
  var search = $('.cate-tags');
  var searchs = [];
  search.each(function(){
    searchs.push(this.value);
  });
  var result = $('.search-container').map(function() {
    var row =[];
    row.push($(this).find('input[name="tag-cat"]').val());
    return [row]
  }).get();
  $('.published').addClass('no-block')
  for (var i = 0; i < result.length; i++) {
    $.each(result[i], function(index, value) {
      $.each(searchs, function(id, content) {
        if (index == id) {
          if (value == content) {
            console.log(value, content);
            $('.publish-' + i).removeClass('no-block')
          }
        }
      })
    })
  }
})
//PAIS COMPARA
$('#select-pais').on('change', function(e) {
  var search = $('.pais-tags');
  var searchs = [];
  search.each(function(){
    searchs.push(this.value);
  });
  var result = $('.search-container').map(function() {
    var row =[];
    row.push($(this).find('input[name="tag-pai"]').val());
    return [row]
  }).get();
  $('.published').addClass('no-block')
  for (var i = 0; i < result.length; i++) {
    $.each(result[i], function(index, value) {
      $.each(searchs, function(id, content) {
        if (index == id) {
          if (value == content) {
            console.log(value, content);
            $('.publish-' + i).removeClass('no-block')
          }
        }
      })
    })
  }
})
//CIUDAD COMPARA
$('#select-ciud').on('change', function(e) {
  var search = $('.ciud-tags');
  var searchs = [];
  search.each(function(){
    searchs.push(this.value);
  });
  var result = $('.search-container').map(function() {
    var row =[];
    row.push($(this).find('input[name="tag-ciu"]').val());
    return [row]
  }).get();
  $('.published').addClass('no-block')
  for (var i = 0; i < result.length; i++) {
    $.each(result[i], function(index, value) {
      $.each(searchs, function(id, content) {
        if (index == id) {
          if (value == content) {
            console.log(value, content);
            $('.publish-' + i).removeClass('no-block')
          }
        }
      })
    })
  }
})

function configura_busqueda(v) {
  configura_busqueda.called = true;
  var value = v;
  $.ajax({
    type: "POST",
    url: "https://searchtwome.com/data/php/search/search-configurable.php",
    data: {
      value: value
    },
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      configura_busqueda.called = false;
      var txt = "", renta, i = 0;
      $.each(data, function(index, content) {
        if (content.success) {
          if (content.ren == 1) { renta = "renta"} else {renta = "venta"}
          txt += '<div class="column is-4 published publish-' + i + '">';
            txt += '<a href="muestra.php?pro=' + content.pro_index + '" target="_blank" class="show-on-map is-relative" val="' + content.pro_index + '">';
              txt += '<div class="box is-relative bo-r-no pa-no buscar-box">';
                txt += '<img src="https://searchtwome.com/data/assets/pro_img/' + content.pro_index + '/' + content.pro_index + '_0.png">';
                txt += '<h5 class="pa-half te-shadow-grey search-container">';
                  txt += '<span class="has-background-link is-size-7 pa-half bo-r-five shadow-grey">' + money_format(content.pre) + '&nbsp;' + content.cur + '</span>';
                  txt += '<p class="fo-w-b pa-t-5p">' + content.nom + '</>';
                  txt += '<p class="fo-s-12 no-block">' + content.col + ', ' + content.ciu + ', ' + content.est + '</p>';
                  txt += '<p class="fo-s-12 no-block">' + content.pai + '</p>';
                  txt += '<input type="hidden" class="no-block span-tags" name="tag-ren" value="' + content.ren + '">';
                  txt += '<input type="hidden" class="no-block span-tags" name="tag-cat" value="' + content.cate_index + '">';
                  txt += '<input type="hidden" class="no-block span-tags" name="tag-pai" value="' + content.pai + '">';
                  txt += '<input type="hidden" class="no-block span-tags" name="tag-ciu" value="' + content.ciu + '">';
                  txt += '<input type="hidden" class="no-block span-tags" name="tag-opc" value="' + content.opc + '">';
                  txt += '<p class="no-block pa-t-5p">';
                    txt += '<span class="has-background-success is-size-7 ma-r-5p pa-half bo-r-five shadow-grey">' + renta + '</span>';
                    txt += '<span class="has-background-link is-size-7 pa-half bo-r-five shadow-grey">' + content.cat + '</span>';
                  txt += '</p>';
                txt += '</h5>';
              txt += '</div>';
            txt += '</a>';
          txt += '</div>';
          i++
        }
      })
      $('#buscar-results').html(txt);

      //ACTION ON HOVER PARA MOSTRARLO EN EL MAPA CUENTA CON DELAY DE 3 SEGS
      var longpress;
      $('.show-on-map')
      .mouseenter(function() {
        var pro_index = $(this).attr('val');
        $(this).addClass('loading-box');
        longpress=true;
        setTimeout(function() {
          if (longpress) {
            //SHOW ON MAP FUNCTION
            if(muestra_on_map.called) {
            } else {
              muestra_on_map(pro_index)
            }
          }
          $('.show-on-map').removeClass('loading-box');
        }, 3000)
      })
      .mouseleave(function() {
        $('.show-on-map').removeClass('loading-box');
        longpress=false
      })

      //FUNCTION MUESTRA ON MAPA MARKER Y VA HACIA EL
      function muestra_on_map(e) {
        muestra_on_map.called = true
        //MARKER LAYER NEW FOR BUSCAR FEED
        $.ajax({
          type: "POST",
          url: "https://searchtwome.com/data/php/products/products-marker.php",
          data: {
            id : e
          },
          async:true,
          dataType : 'json',
          crossDomain: true,
          context: document.body,
          cache: false,
          success: function(data) {
            $.each(data, function (name, value) {
              if (value.success == true) {
                muestra_on_map.called = false
                //VOLAMOS AL NUEVO CENTRO
                map.flyTo(new L.LatLng(value.lat, value.lng), 17);
                L.marker([value.lat, value.lng], {icon: iconNear}).addTo(markerFeed).on('click', function(e) {
                  carga_propiedad(value.pro_index);
                });
              }
            })
          },
          error: function(xhr, tst, err) {
            muestra_on_map.called = false
            toast(err)
          }
        })
      }
    },
    error: function(xhr, tst, err) {
      configura_busqueda.called = false;
      modal(err)
    }
  });
}

function carga_search_slider(e) {
  var slider =  document.getElementById('slider1');
  var output = document.getElementById('number1');
  output.innerHTML = slider.value

  slider.oninput = function() {
    output.innerHTML = money_format(this.value)
  }
}

function carga_search_cat(e) {
  $.ajax({
    type: "POST",
    url: "https://searchtwome.com/data/php/products/products-categorias.php",
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      $('#select-cate').html('');
      $('#select-cate').append('<option selected disabled value=0>Categor&iacute;as</option>');
      $.each(data, function(index, content){
        if (content.success) {
          $('#select-cate').append('<option value="' + content.cate_index + '">' + content.nom + '</option>');
        }
      })
    },
    error: function(xhr, tst, err) {
      console.log(err);
    }
  });
  carga_search_pais()
}

function carga_search_pais(e) {
  $.ajax({
    type: "POST",
    url: "https://searchtwome.com/data/php/products/products-paises.php",
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      $('#select-pais').html('');
      $('#select-pais').append('<option selected disabled value=0>Pa&iacute;ses</option>');
      $.each(data, function(index, content){
        if (content.success) {
          $('#select-pais').append('<option value="' + String(content.pai) + '">' + content.pai + '&nbsp<b class="hast-text-grey">(' + content.num + ')</option>');
        }
      });
      $('#select-pais').on('change',function(){
        var pai = $(this).val();
        carga_search_ciud(pai);
      })
    },
    error: function(xhr, tst, err) {
      console.log(err);
    }
  });
  carga_search_opc();
}

function carga_search_ciud(e) {
  var pai = e;
  $.ajax({
    type: "POST",
    url: "https://searchtwome.com/data/php/products/products-ciudades.php",
    data: {
      pai : pai
    },
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      $('#select-ciud').html('');
      $('#select-ciud').append('<option selected disabled value=0>Ciudades</option>');
      $.each(data, function (index, content) {
        if (content.success) {
          $('#select-ciud').append('<option value="' + String(content.ciu) + '">' + content.ciu + '&nbsp<b class="hast-text-grey">(' + content.num + ')</b></option>')
        }
      })
    },
    error: function(xhr, tst, err) {
      console.log(err);
    }
  })
}

function carga_search_opc(e) {
  $.ajax({
    type: "POST",
    url: "https://searchtwome.com/data/php/products/products-opc.php",
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      $('#select-opc').html('');
      $('#select-opc').append('<option selected disabled value=0>Opciones</option>');
      $.each(data, function(id,content){
        $('#select-opc').append('<option value=' + content.id + '>' + content.name + '</option>')
      })
    },
    error: function(xhr, tst, err) {
      console.log(err);
    }
  })
}
