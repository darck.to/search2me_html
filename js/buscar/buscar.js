$(document).ready(function() {
  //CASCADE LOAD filter--results--map
  carga_navigator();
  carga_footer();

  translate();
})

function carga_navigator(e) {
  $('#navigator').load('templates/navigator/navigator-main.php');
  //CASCADE LOAD
  carga_buscar_filter();
}

function carga_buscar_filter(e) {
  $('#buscar-filter').load('templates/buscar/buscar-filter.html');
  //CASCADE LOAD RESULTS
}

function carga_footer(e) {
  $('#footer').load('templates/footer/footer.html');
}

function carga_busqueda_renta(renta, tipo, clave) {
  var ren = renta;
  var tip = tipo;
  var cla = clave;
  var form_data = new FormData();
  if (tip == 1) {
    form_data.append('cate_index', cla);
  } else if (tip == 2) {
    form_data.append('perf_index', cla);
  }
  form_data.append('renta', ren);
  $.ajax({
    type: "POST",
    url: "https://searchtwome.com/data/php/search/search-filters.php",
    data: form_data,
    contentType: false,
    processData: false,
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      var txt = "", renta, i = 0;
      $.each(data, function(index, content) {
        if (content.success) {
          if (content.ren == 1) { renta = "renta"} else {renta = "venta"}
          txt += '<div class="column is-4 published publish-' + i + '">';
            txt += '<a href="muestra.php?pro=' + content.pro_index + '" target="_blank" class="show-on-map is-relative" val="' + content.pro_index + '">';
              txt += '<div class="box is-relative bo-r-no pa-no buscar-box">';
                txt += '<img src="https://searchtwome.com/data/assets/pro_img/' + content.pro_index + '/' + content.pro_index + '_0.png">';
                txt += '<h5 class="pa-half te-shadow-grey search-container">';
                  txt += '<span class="has-background-link is-size-7 pa-half bo-r-five shadow-grey">' + money_format(content.pre) + '&nbsp;' + content.cur + '</span>';
                  txt += '<p class="fo-w-b pa-t-5p">' + content.nom + '</>';
                  txt += '<p class="fo-s-12 no-block">' + content.col + ', ' + content.ciu + ', ' + content.est + '</p>';
                  txt += '<p class="fo-s-12 no-block">' + content.pai + '</p>';
                  txt += '<input type="hidden" class="no-block span-tags" name="tag-ren" value="' + content.ren + '">';
                  txt += '<input type="hidden" class="no-block span-tags" name="tag-cat" value="' + content.cate_index + '">';
                  txt += '<input type="hidden" class="no-block span-tags" name="tag-pai" value="' + String(content.pai) + '">';
                  txt += '<input type="hidden" class="no-block span-tags" name="tag-ciu" value="' + String(content.ciu) + '">';
                  txt += '<input type="hidden" class="no-block span-tags" name="tag-opc" value="' + content.opc + '">';
                  txt += '<p class="no-block pa-t-5p">';
                    txt += '<span class="has-background-success is-size-7 ma-r-5p pa-half bo-r-five shadow-grey">' + renta + '</span>';
                    txt += '<span class="has-background-link is-size-7 pa-half bo-r-five shadow-grey">' + content.cat + '</span>';
                  txt += '</p>';
                txt += '</h5>';
              txt += '</div>';
            txt += '</a>';
          txt += '</div>';
          i++
        }
      })
      $('#buscar-results').html(txt);

      //ACTION ON HOVER PARA MOSTRARLO EN EL MAPA CUENTA CON DELAY DE 3 SEGS
      var longpress;
      $('.show-on-map')
      .mouseenter(function() {
        var pro_index = $(this).attr('val');
        $(this).addClass('loading-box');
        longpress=true;
        setTimeout(function() {
          if (longpress) {
            //SHOW ON MAP FUNCTION
            if(muestra_on_map.called) {
            } else {
              muestra_on_map(pro_index)
            }
          }
          $('.show-on-map').removeClass('loading-box');
        }, 3000)
      })
      .mouseleave(function() {
        $('.show-on-map').removeClass('loading-box');
        longpress=false
      })

    },
    error: function(xhr, tst, err) {
      console.log(err);
    }
  })


}

//MAPA//
//AQUI COMIENZA EL SHOW//
if (map) {
} else {
  //MAP
  var map = L.map('buscar-map').setZoom(13);
  //MARKER LAYER
  var markerGroup = L.layerGroup().addTo(map);
  var markerFeed = L.layerGroup().addTo(map);
}

var iconNear = L.icon({
  iconUrl: 'img/leaf/near.png',
  iconSize: [35, 35]
});

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  attribution: ''
}).addTo(map);

map.on("moveend", function () {
  markerGroup.clearLayers();
  markerFeed.clearLayers();
  newCenter()
});

//NUEVO CENTRO
function newCenter() {

  coords = L.latLng(map.getCenter());
  var lat = coords.lat;
  var lng = coords.lng;

  localStorage.setItem("lat",lat);
  localStorage.setItem("lng",lng);

  var latNow = localStorage.getItem("lat");
  var lngNow = localStorage.getItem("lng");

  //BUSCA COINCIDENCIAS EN 50KLM A LA REDONDA
  var dis = localStorage.getItem("range");
  dis = dis * 1.609344; //KLM

  var stringaUth_key = localStorage.getItem("aUth_key");
  var stringaUth_user = localStorage.getItem("aUth_user");
  $.ajax({
    type: "POST",
    url: "https://searchtwome.com/data/php/main/main-coincidencias.php",
    data: {
      auth : stringaUth_key,
      user : stringaUth_user,
      lat : latNow,
      lng : lngNow,
      dis : dis
    },
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      n = 0;
      $.each(data, function (name, value) {
        if (value.success == false) {
        } else {
          n++;
          L.marker([value.lat, value.lng], {icon: iconNear}).addTo(markerGroup).on('click', function(e) {
            carga_propiedad(value.pro_index);
          });
        }
      });
      $('#noNear').html(n);
    },
    error: function(xhr, tst, err) {
      toast(err)
    }
  })

}

//SIN FILTRO
function onLocationFound(e) {

  L.marker(e.latlng).addTo(map);
  coords = L.latLng(map.getCenter());
  var lat = coords.lat;
  var lng = coords.lng;

  localStorage.setItem("lat",lat);
  localStorage.setItem("lng",lng);

  var latNow = localStorage.getItem("lat");
  var lngNow = localStorage.getItem("lng");

  //BUSCA COINCIDENCIAS EN 50KLM A LA REDONDA
  localStorage.setItem("range", 35);
  var dis = localStorage.getItem("range");
  dis = dis * 1.609344; //KLM

  var stringaUth_key = localStorage.getItem("aUth_key");
  var stringaUth_user = localStorage.getItem("aUth_user");
  $.ajax({
    type: "POST",
    url: "https://searchtwome.com/data/php/main/main-coincidencias.php",
    data: {
      auth : stringaUth_key,
      user : stringaUth_user,
      lat : latNow,
      lng : lngNow,
      dis : dis
    },
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      n = 0;
      $.each(data, function (name, value) {
        if (value.success == false) {
        } else {
          n++;
          L.marker([value.lat, value.lng], {icon: iconNear}).addTo(markerGroup).on('click', function(e) {
            carga_propiedad(value.pro_index);
          });
        }
      });
      $('#noNear').html(n);
    },
    error: function(xhr, tst, err) {
      toast(err)
    }
  })

}

//CON FILTRO
function onLocationFilter(e) {
  var latNow = localStorage.getItem("lat");
  var lngNow = localStorage.getItem("lng");

  L.marker(e.latlng).addTo(map);

  //BUSCA COINCIDENCIAS EN 50KLM A LA REDONDA
  var dis = localStorage.getItem("range");
  dis = dis * 1.609344; //KLM

  var stringaUth_key = localStorage.getItem("aUth_key");
  var stringaUth_user = localStorage.getItem("aUth_user");

  var n = 0;
  var marcadores = JSON.parse(localStorage.getItem('markers'));
  $.each(marcadores, function (name, value) {
    n++;
    L.marker([value.lat, value.lng], {icon: iconNear}).addTo(map).on('click', function(e) {
    });
  });
  $('#noNear').html(n);
}

function onLocationError(e) {
  alert(e.message);
}

//FUNCTION MUESTRA ON MAPA MARKER Y VA HACIA EL
function muestra_on_map(e) {
  muestra_on_map.called = true
  //MARKER LAYER NEW FOR BUSCAR FEED
  $.ajax({
    type: "POST",
    url: "https://searchtwome.com/data/php/products/products-marker.php",
    data: {
      id : e
    },
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      $.each(data, function (name, value) {
        if (value.success == true) {
          muestra_on_map.called = false
          //VOLAMOS AL NUEVO CENTRO
          map.flyTo(new L.LatLng(value.lat, value.lng), 17);
          L.marker([value.lat, value.lng], {icon: iconNear}).addTo(markerFeed).on('click', function(e) {
            carga_propiedad(value.pro_index);
          });
        }
      })
    },
    error: function(xhr, tst, err) {
      muestra_on_map.called = false
      toast(err)
    }
  })
}

map.on('locationfound', onLocationFound);
map.on('locationerror', onLocationError);

map.locate({setView: true, maxZoom: 16});

function carga_propiedad(e) {
  var pro_index = e;
  $.ajax({
    type: "POST",
    url: "https://searchtwome.com/data/php/products/products-show.php",
    data: {
      pro_index : pro_index
    },
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      var txt = "", renta;
      $.each(data, function(index, content) {
        if (content.success) {
          if (content.ren == 1) { renta = "renta"} else {renta = "venta"}
          txt += '<div class="box">';
          txt += '<a href="muestra.php?pro=' + pro_index + '" target="_blank">';
          txt += '<div class="box is-relative bo-r-no pa-no map-box">';
          txt += '<img src="https://searchtwome.com/data/assets/pro_img/' + pro_index + '/' + pro_index + '_0.png">';
          txt += '<h5 class="pa-half te-shadow-grey">';
          txt += '<span class="has-background-link is-size-7 pa-half bo-r-five shadow-grey">' + money_format(content.pre) + '&nbsp;' + content.cur + '</span>';
          txt += '<p class="fo-w-b pa-t-5p">' + content.nom + '</>';
          txt += '<p class="fo-s-12 no-block">' + content.col + ', ' + content.ciu + ', ' + content.est + '</p>';
          txt += '<p class="fo-s-12 no-block">' + content.pai + '</p>';
          txt += '<p class="no-block pa-t-5p">';
          txt += '<span class="has-background-success is-size-7 ma-r-5p pa-half bo-r-five shadow-grey">' + renta + '</span>';
          txt += '<span class="has-background-link is-size-7 pa-half bo-r-five shadow-grey">' + content.cat + '</span>';
          txt += '</p>';
          txt += '</h5>';
          txt += '</div>';
          txt += '</a>';
          txt += '</div>';
        }
      })
      modal(txt);
    },
    error: function(xhr, tst, err) {
      console.log(err);
    }
  })
}
