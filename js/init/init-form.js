$(document).ready(function() {

  $('.buttonRegisterForm').on('click', function(e){
    $('.modal-content').load('templates/init/init-register.html');
  })

  //LOGIN DEL USUARIO
  $('#initLoginUser').submit(function(event) {
    event.preventDefault();

    var formData = new FormData(document.getElementById("initLoginUser"));
    var formMethod = $(this).attr('method');
    var rutaScrtip = $(this).attr('action');

    var request = $.ajax({
      url: rutaScrtip,
      method: formMethod,
      data: formData,
      contentType: false,
      processData: false,
      async:true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
    });
    // handle the responses
    request.done(function(data) {
      var aprobacion, key, user;
      $.each(data, function (name, value) {
        aprobacion = value.success;
        key = value.aUth_key;
        user = value.aUth_user;
      });
      if (aprobacion == true) {
        $('.logButton').addClass('is-loading');
        localStorage.setItem("aUth_key", key);
        localStorage.setItem("aUth_user", user);
        localStorage.setItem('currency_symbol','MXN');
        carga_session();
      } else {
        console.log(data.error);
      }
    })
    request.fail(function(jqXHR, textStatus) {
      console.log(textStatus);
    })
    request.always(function(data) {
      // clear the form
      $('#initLoginUser').trigger('reset');
    });

  })

  function carga_session(e) {
    $.ajax({
      type: 'POST',
      url: 'templates/init/init-start.php',
      data: {
        a: 1
      },
      cache: false,
      success: function(data) {
        toast('Session Iniciada');
        setTimeout(location.reload.bind(location), 1000);
      },
      error: function(xhr, tst, err) {
        console.log(err);
      }
    })
  }

  $('.buttonRecoverPass').on('click', function(e) {
    var content = '<div class="box ma-one pa-one">';
      content += '<div class="has-text-centered">';
        content += '<h1 class="has-text-centered has-text-danger is-size-4 fo-w-l">' + translate_string('init_mod_h1') + '</h1>';
        content += '<input id="i-mai" type="text" class="input" placeholder=' + translate_string('init_mod_inp') + '/>';
        content += '<h1 class="has-text-right"><span class="is-size-7 has-text-grey ma-one pa-one">' + translate_string('init_mod_spa') + '</span></h1>';
        content += '<button class="button is-primary ma-one recoverPassword">' + translate_string('init_mod_but') + '</button>';
      content += '</div>';
    content += '</div>';
    modal(content)

    $('.recoverPassword').on('click', function(e){
      var mai = $('#i-mai').val();
      $.ajax({
        type: "POST",
        url: "https://searchtwome.com/data/php/init/init-recover-password.php",
        data: {
          mai: mai
        },
        async:true,
        dataType : 'json',
        crossDomain: true,
        context: document.body,
        cache: false,
        success: function(data) {
          $.each(data, function(name, content){
            if(content.success == true) {
              toast(content.message);
            } else {
              toast(content.message);
            }
          })
        },
        error: function(xhr, tst, err) {
          toast(data);
        }
      })
    })
  })
  translate()
})
