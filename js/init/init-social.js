//FACEBOOK
window.fbAsyncInit = function() {
  FB.init({
    appId      : '1411114049075956',
    cookie     : true,                     // Enable cookies to allow the server to access the session.
    xfbml      : true,                     // Parse social plugins on this webpage.
    version    : 'v2.8',
  });
  FB.getLoginStatus(function(response) {   // Called after the JS SDK has been initialized.
    statusChangeCallback(response);        // Returns the login status.
  });
};

(function(d, s, id) {                      // Load the SDK asynchronously
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "https://connect.facebook.net/en_US/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

FB.getLoginStatus(function(response) {
  if (response.status == 'connected') {
    getCurrentUserInfo(response);
    comparaLogin(response.email)
  } else {
    getCurrentUserInfo(response)
  }
});

function getCurrentUserInfo() {
  FB.api('/me?fields=id,name,email,permissions', function(userInfo) {
    //console.log(userInfo.name + ': ' + userInfo.email);
  });
}

function loginFacebook(e) {
  FB.login(function(response) {
    if (response.authResponse) {
     //console.log('Welcome!  Fetching your information.... ');
     FB.api('/me?fields=id,name,email,permissions', function(response) {
       //console.log('Good to see you, ' + response.name + ', ' + response.email);
       //COMPARA LOGIN con REGISTROS LOCALES
       comparaLogin(response.email)
     });
    } else {
     //console.log('User cancelled login or did not fully authorize.');
    }
  }, { scope: 'email' });
}

//GOOGLE
function onSignIn(googleUser) {
  var profile = googleUser.getBasicProfile();
  //console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
  //console.log('Name: ' + profile.getName());
  //console.log('Image URL: ' + profile.getImageUrl());
  //console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
  //COMPARA LOGIN con REGISTROS LOCALES
  comparaLogin(profile.getEmail())
}

//FUNCTIONS DE COMPARACION CON LOCAL/SOCIAL
function comparaLogin(e) {
  var email = e;
  $.ajax({
    type: 'POST',
    url: 'data/php/init/init-social.php',
    data: {
      email: email
    },
    cache: false,
    success: function(data) {
      var aprobacion, key, user;
      $.each(data, function (name, value) {
        aprobacion = value.success;
        key = value.aUth_key;
        user = value.aUth_user;
      });
      if (aprobacion == true) {
        $('.logButton').addClass('is-loading');
        localStorage.setItem("aUth_key", key);
        localStorage.setItem("aUth_user", user);
        localStorage.setItem('currency_symbol','MXN');
        carga_session();
      } else {
        //SI USUARIO NO ESTA REGISTRADO, OFRECERLE REGISTRARSE
        $('.modal-content').load('templates/init/init-register.html', function() {
          $('#r-mai').val(email);
          //console.log(email);
        });
        toast('Registrate con tus datos')
      }
    },
    error: function(xhr, tst, err) {
      console.log(err);
    }
  })
}

function carga_session(e) {
  $.ajax({
    type: 'POST',
    url: 'templates/init/init-start.php',
    data: {
      a: 1
    },
    cache: false,
    success: function(data) {
      toast('Session Iniciada');
      setTimeout(location.reload.bind(location), 1000);
    },
    error: function(xhr, tst, err) {
      console.log(err);
    }
  })
}