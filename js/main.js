$(document).ready(function() {
  //CASCADE LOAD navigator--searchmain--masonryfavs--categorias
  carga_navigator();
  carga_footer();
})

function carga_navigator(e) {
  $('#navigator').load('templates/navigator/navigator-main.php');
  //CASCADE LOAD
  carga_search_main();
}

function carga_search_main(e) {
  $('#search-main').load('templates/search/search-main.html');
  //CASCADE LOAD
  carga_masonry_main();
}

function carga_masonry_main(e) {
  $('#search-masonry').load('templates/search/search-masonry.html');
  //CASCADE LOAD
  carga_categories_search();
}

function carga_categories_search(e) {
  $('#search-categories').load('templates/search/search-categories.html');
  carga_agencies_search();
}

function carga_agencies_search(e) {
  $('#search-agencies').load('templates/search/search-agencies.html');
}

function carga_footer(e) {
  $('#footer').load('templates/footer/footer.html');
}
