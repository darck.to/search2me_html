$(document).ready(function() {
  carga_navigator()
  carga_footer()

  translate()

  $('.changeCurrency').on('click', function(e) {
    var txt = $('.product-symbol').html();
    console.log(txt);
    if (txt == 'MXN') {
      localStorage.setItem('currency_symbol','USD');
      $('.product-symbol').html('USD')
    } else if (txt == 'USD') {
      localStorage.setItem('currency_symbol','MXN');
      $('.product-symbol').html('MXN')
    }
  })

});

function carga_navigator(e) {
  $('#navigator').load('templates/navigator/navigator-main.php');
}

function carga_footer(e) {
  $('#footer').load('templates/footer/footer.html');
}
