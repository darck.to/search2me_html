$(document).ready(function() {
  // Check for click events on the navbar burger icon
  $(".navbar-burger").click(function() {
    // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
    $(".navbar-burger").toggleClass("is-active");
    $(".navbar-menu").toggleClass("is-active");
  });
  //CARGA MENU
  carga_navigator_menu()
})

function onLoad() {
  gapi.load('auth2', function() {
    gapi.auth2.init();
  });
}

function carga_navigator_menu(e) {
  $.ajax({
    type: "POST",
    url: "https://searchtwome.com/data/php/products/products-categorias.php",
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      $.each(data, function(index, content){
        if (content.success) {
          $('#menu-cat-venta').append('<a class="navbar-item" href="buscar.php?renta=0&cate_index=' + content.cate_index + '">' + content.nom + ' en venta</a>');
          $('#menu-cat-renta').append('<a class="navbar-item" href="buscar.php?renta=1&cate_index=' + content.cate_index + '">' + content.nom + ' en renta</a>')
        }
      })
    },
    error: function(xhr, tst, err) {
      console.log(err)
    }
  })
}

$('.add-property-nav').on('click', function(e) {
  localStorage.removeItem('edit_id');
  modal('');
  $('.modal-content').addClass('he-100v5 ove-no');
  $('.modal-content').append('<div class="box he-100p"></div>');
  $('.modal-content .box').append('<div class="content he-100p ove-scroll"></div>')
  $('.modal-content .box .content').load('templates/products/products-add.html')
})

$('#session-nueva').on('click', function(e){
  var stringaUth_key = localStorage.getItem("aUth_key");
  var stringaUth_user = localStorage.getItem("aUth_user");
  comprueba_login(stringaUth_key,stringaUth_user);
  function comprueba_login(a,u) {
    $.ajax({
      type: 'POST',
      url: 'https://searchtwome.com/data/php/init/init-comprueba-auth.php',
      data: {
        auth : a,
        user : u
      },
      async:true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        var aprobacion
        $.each(data, function (name, value) {
          aprobacion = value.success;
        });
        if (aprobacion == true) {
          carga_session()
        } else {
          modal();
          $('.modal-content').load('templates/init/init-form.html');
        }
      },
      error: function(xhr, tst, err) {
        console.log(err);
      }
    })
  }

  function carga_session(e) {
    $.ajax({
      type: 'POST',
      url: 'templates/init/init-start.php',
      data: {
        a: 1
      },
      cache: false,
      success: function(data) {
        toast('Session Iniciada');
        setTimeout(location.reload.bind(location), 1000);
      },
      error: function(xhr, tst, err) {
        console.log(err);
      }
    })
  }
})

//CIERRA SESION Facebook
function statusChangeCallback(response) {  // Called with the results from FB.getLoginStatus().
  console.log('statusChangeCallback');
  console.log(response);                 // The current login status of the person.
}
window.fbAsyncInit = function() {
  FB.init({
    appId      : '1411114049075956',
    cookie     : true,                     // Enable cookies to allow the server to access the session.
    xfbml      : true,                     // Parse social plugins on this webpage.
    version    : 'v2.8',
  });
  FB.getLoginStatus(function(response) {   // Called after the JS SDK has been initialized.
    statusChangeCallback(response);        // Returns the login status.
  });
};
(function(d, s, id) {                      // Load the SDK asynchronously
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "https://connect.facebook.net/en_US/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
//SESSION CLOSE
$('#session-close').on('click', function(e){
  //FACEBOOK FIRST IF ANY
  FB.logout(function(response) {
     console.log(response)
  })
  //GOOGLE THEN IF ANY
  var auth2 = gapi.auth2.getAuthInstance();
  auth2.signOut().then(function () {
    console.log('User signed out.');
  });
  //CIERRA SESION PHP
  $.ajax({
    type: 'POST',
    url: 'templates/init/init-start.php',
    data: {
      a: 0
    },
    cache: false,
    success: function(data) {
      toast('Session Cerrada');
      localStorage.removeItem("aUth_key");
      localStorage.removeItem("aUth_user");
      setTimeout(location.reload.bind(location), 1000);
    },
    error: function(xhr, tst, err) {
      console.log(err);
    }
  })
})
