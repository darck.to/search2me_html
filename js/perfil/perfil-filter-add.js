$(document).ready(function() {

  $('#slider2').val(money_format(10));
  var slider2 =  document.getElementById('slider2');
  var output2 = document.getElementById('number2');

  slider2.oninput = function() {
    output2.innerHTML = money_format(this.value);
  }

  //SI ES EDICION
  var filter;
  if (localStorage.getItem("edit_id") != null) {
    carga_datos_edicion()
  } else {
    carga_opciones();
    carga_categorias()
  }
  translate()
})

function carga_datos_edicion(callback) {
  //SI ES EDICION
  if (localStorage.getItem("edit_id") != null) {
    filter_index = localStorage.getItem("edit_id");
    var stringaUth_key = localStorage.getItem("aUth_key");
    var stringaUth_user = localStorage.getItem("aUth_user");
    $.ajax({
      type: "POST",
      url: "https://searchtwome.com/data/php/user/user-filter.php",
      data: {
        auth : stringaUth_key,
        user : stringaUth_user,
        id : filter_index
      },
      async: true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        carga_opciones(data);
        carga_categorias(data)
      },
      error: function(xhr, tst, err) {
        toast(err)
      }
    })//END first
  }//END IF
}

function carga_opciones(e) {
  $.ajax({
    type: "POST",
    url: "https://searchtwome.com/data/php/products/products-opc.php",
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      $('#product-opc').html('');
      var opc = "";
      //SI ES EDICION
      if (localStorage.getItem("edit_id") != null) {
        $.each(e,function(index,value){
          
        })
      }
      $.each(data, function(id,content){
        opc += '<p>';
          opc += '<label class="checkbox is-relative filter-check pa-5" data-text="' + content.name + '">';
            opc += '<input type="checkbox" class="check-' + content.id + '">';
            opc += '<i class="has-text-grey-dark ' + content.icon + ' pa-half"></i>';
            opc += content.name;
            if (content.type == "integer") { opc += '<input class="input is-hidden integer-input input-opc-filter int-' + content.id + ' is-p-a-t-10p" type="number" value="1">';}
          opc += '</label>';
        opc += '</p>'
      })
      $('#product-opc').append(opc)
      //TOUCH OPTIONS
      $('.filter-check').on('click', function(e){
        $('.filter-check').children('.input-opc-filter').addClass('is-hidden');
        $(this).children('.input-opc-filter').removeClass('is-hidden');
        toast($(this).data('text'))
      });
    },
    error: function(xhr, tst, err) {
      toast(err)
    }
  })
  $(".integer-input").on("keypress keyup blur",function (event) {
   $(this).val($(this).val().replace(/[^\d].+/, ""));
    if ((event.which < 48 || event.which > 57)) {
      event.preventDefault()
    }
  })
}

function carga_categorias(e) {
  $.ajax({
    type: "POST",
    url: "https://searchtwome.com/data/php/products/products-categorias.php",
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      $('#categorias-select').html('');
      $('#categorias-select').append('<option value=0>' + translate_string('search_option_cat') + '</option>');
      $.each(data, function (name, value) {
        $('#categorias-select').append('<option value=' + value.cate_index + '>' + value.nom + '&nbsp<b class="hast-text-grey">(' + value.num + ')</b></option>');
      });
      $('#categorias-select').toggleClass('is-loading')
    },
    error: function(xhr, tst, err) {
      toast(err)
    }
  })
}

$('#save-filter').on('click', function(e){
  $(this).addClass('is-loading');
  $(this).addClass('has-text-warning');

  var ren = $('input[name="ren"]:checked').val();
  var categorias = $('#categorias-select').val()

  if ($('input.check-cuartos').is(':checked')) { var cuartos = $('.int-cuartos').val(); } else { var cuartos = 0; }
  if ($('input.check-jacuzzi').is(':checked')) { var jacuzzi = true; } else { var jacuzzi = false; }
  if ($('input.check-air').is(':checked')) { var air = true; } else { var air = false; }
  if ($('input.check-jardin').is(':checked')) { var jardin = true; } else { var jardin = false; }
  if ($('input.check-chimenea').is(':checked')) { var chimenea = true; } else { var chimenea = false; }
  if ($('input.check-banos').is(':checked')) { var banos = $('.int-banos').val(); } else { var banos = 0; }
  if ($('input.check-cochera').is(':checked')) { var cochera = $('.int-cochera').val(); } else { var cochera = 0; }
  if ($('input.check-piscina').is(':checked')) { var piscina = true; } else { var piscina = false; }
  if ($('input.check-terraza').is(':checked')) { var terraza = true; } else { var terraza = false; }
  if ($('input.check-balcon').is(':checked')) { var balcon = true; } else { var balcon = false; }
  if ($('input.check-seguridad').is(':checked')) { var seguridad = true; } else { var seguridad = false; }
  if ($('input.check-recepcion').is(':checked')) { var recepcion = true; } else { var recepcion = false; }
  if ($('input.check-gimnasio').is(':checked')) { var gimnasio = true; } else { var gimnasio = false; }

  var stringaUth_key = localStorage.getItem("aUth_key");
  var stringaUth_user = localStorage.getItem("aUth_user");
  var precio = $('#slider2').val();
  $.ajax({
    type: "POST",
    url: "https://searchtwome.com/data/php/main/main-filter-guarda.php",
    data: {
      auth : stringaUth_key,
      user : stringaUth_user,
      ren : ren,
      precio : precio,
      categorias : categorias,
      cuartos : cuartos,
      jacuzzi : jacuzzi,
      air : air,
      jardin : jardin,
      chimenea : chimenea,
      banos : banos,
      cochera : cochera,
      piscina : piscina,
      terraza : terraza,
      balcon : balcon,
      seguridad : seguridad,
      recepcion : recepcion,
      gimnasio : gimnasio
    },
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    success: function(data) {
      toast(translate_string('toast_filter_save'));
    },
    error: function(xhr, tst, err) {
      toast(err)
    }
  })
})
