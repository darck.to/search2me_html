$(document).ready(function() {
  carga_main();
  
  $('.img-avatar-edit').on('click', function(e) {
    //insert html code for img uploader
    var txt = '<section class="section has-background-white bo-r-five">';
      txt += '<h2 class="is-size-4 has-text-centered pa-one">Reemplaza tu foto de perfil</h2>';
      txt += '<div class="content has-text-centered">';
        txt += '<div class="file has-name is-boxed is-block">';
          txt += '<label class="file-label">';
            txt += '<input id="img-ava-upload" class="file-input" type="file" name="img" accept="image/*">';
            txt += '<span class="file-cta">';
              txt += '<span class="file-icon">';
                txt += '<i class="fas fa-upload"></i>';
              txt += '</span>';
              txt += '<span class="file-label">';
                txt += 'Choose a file…';
              txt += '</span>';
            txt += '</span>';
          txt += '</label>';
        txt += '</div>';
        txt += '<div class="content has-text-centered">';
          txt += '<img id="img-holder-upload" class="ma-one" src="" alt="image uploader" width="200" height="220">';
        txt += '</div>';
        txt += '<div class="content has-text-centered">';
          txt += '<button class="button ma-one but-ava-replace">Reemplazar</button>';
        txt += '</div>';
      txt += '</div>';
    txt += '</section>';
    //INSERTA EN MODAL
    modal(txt);
    var reader = new FileReader();
    reader.onload = function (e) {
      $('#img-holder-upload').attr('src', e.target.result);
    }
    function readURL(input) {
      if (input.files && input.files[0]) {
        reader.readAsDataURL(input.files[0]);
      }
    }
    $("#img-ava-upload").change(function(){
      readURL(this);
    });
    //REEMPLAZA AVATAR
    $('.but-ava-replace').click(function(){
      var stringaUth_key = localStorage.getItem("aUth_key");
      var stringaUth_user = localStorage.getItem("aUth_user");
      var form_data = new FormData();
      var file = $('#img-ava-upload')[0].files[0];
      form_data.append('auth',stringaUth_key);
      form_data.append('user', stringaUth_user);
      form_data.append('file',file);
      $.ajax({
        url: 'https://searchtwome.com/data/php/main/main-user-avatar.php',
        type: 'post',
        data: form_data,
        contentType: false,
        processData: false,
        success: function(data){
          $.each(data, function (name, value) {
            if(value.success) {
              toast('Imagen Actualizada');
              setTimeout(location.reload.bind(location), 500)
            }
          })
        },
      });
    });
  })
})

function carga_main(e) {
  var stringaUth_key = localStorage.getItem("aUth_key");
  var stringaUth_user = localStorage.getItem("aUth_user");
  $.ajax({
    type: "POST",
    url: "https://searchtwome.com/data/php/main/main-user.php",
    data: {
      auth : stringaUth_key,
      user : stringaUth_user
    },
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      $.each(data, function (name, value) {
        $('#img-avatar').attr('src',value.img);
        $('#p-nom').html(value.nom + ' ' + value.ape + ' ' + value.mat);
        $('#p-mai').html('<i class="fas fa-at has-text-primary is-pulled-left user-info-icon"></i>&nbsp;' + value.mai);
        $('#p-cel').html('<i class="fas fa-mobile-alt has-text-primary is-pulled-left user-info-icon"></i>&nbsp;' + value.cel);
        $('#p-tel').html('<i class="fas fa-tty has-text-primary is-pulled-left user-info-icon"></i>&nbsp;' + value.tel);
      });
    },
    error: function(xhr, tst, err) {
        console.log(err);
    }
  })
}
