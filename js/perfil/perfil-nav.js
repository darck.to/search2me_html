$(document).ready(function() {
  //CARGA PERFIL NAV
  carga_perfil_nav();
})

//CARGA RESUMEN
$('.carga-resumen').on('click', function(e){
  carga_perfil_nav(e);
  toast('Resumen de cuenta')
})
//CARGA CONTACTOS
$('.carga-contact').on('click', function(e){
  carga_perfil_contact(e);
  $('#perfil-content-box').html('');
  toast('Proximamente...')
})
//CARGA ADMINISTRACION
$('.carga-adm').on('click', function(e){
  var key = $(this).data('src');
  carga_perfil_adm(key);
  $('#perfil-content-box').html('');
  toast('Datos de perfil')
})
//CARGA PLUS
$('.carga-plu').on('click', function(e){
  carga_perfil_plus();
  $('#perfil-content-box').html('');
  toast('Plan plus')
})
//CARGA PUBLICACIONES
$('.carga-pub').on('click', function(e){
  var key = $(this).data('src');
  $('#perfil-content-box').html('');
  carga_perfil_pub(key)
})

function carga_perfil_nav(e) {
  loader($('#perfil-content-tile-main'));
  var stringaUth_key = localStorage.getItem("aUth_key");
  var stringaUth_user = localStorage.getItem("aUth_user");
  $.ajax({
    type: "POST",
    url: "https://searchtwome.com/data/php/products/products-user.php",
    data: {
      auth : stringaUth_key,
      user : stringaUth_user
    },
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      $('#perfil-content-title-main').html('Publicaciones');
      $('#perfil-content-subtitle-main').html(data.length + ' publicaciones');
      var box = "";
      $.each(data, function (name, value) {
        if (value.success) {
          var pro_index = value.pro_index;
          box += '<div class="column is-4 pa-no">';
            box += '<div class="box ma-one pa-no handed product-show" id=' + pro_index + '>';
              box += '<div class="columms ma-no pa-no">';
                box += '<div class="columm ma-no pa-no">';
                  box += '<img class="main-scroll-overlay-img height-170" src="https://searchtwome.com/data/assets/pro_img/' + value.img + '/' + value.img + '_0.png"/>';
                box += '</div>';
                box += '<div class="columm pa-one">';
                  box += '<div class="content">';
                    box += '<h1 class="is-size-5 fo-w-l ma-no">' + value.nom + '</h1>';
                    box += '<h1 class="pa-no ma-no"><small class="is-size-6 has-text-grey-light pa-b-half fo-w-l">&nbspid: ' + value.pro_index + '</small></h1>';
                    box += '<p class="has-text-right has-text-grey-light is-size-7 pa-no ma-no" data-translate="main_p_man">presiona para revisar y editar la publicacion</p>';
                  box += '</div>';
                box += '</div>';
              box += '</div>';
            box += '</div>';
          box += '</div>';
        }
        xloader();
      });
      box += '<div class="column is-12 pa-one"></div>';
      $('#perfil-content-tile-main').html(box);

      $('.product-show').on('click', function(e){
        var pro_index = $(this).attr('id');
        toast(pro_index)
      })
    },
    error: function(xhr, tst, err) {
      console.log(err);
    }
  })
}

function carga_perfil_contact(e) {
  localStorage.removeItem('edit_id');
  $('#perfil-content-title-main').html('Contactos');
  //$('#perfil-content-subtitle-main').html('Número de contactos');
  var box = "";
  box += '<div class="column is-6 pa-no">';
    box += '<h3>Proximamente...</h3>';
  box += '</div>';
  box += '<div class="column is-12 pa-one"></div>';
  $('#perfil-content-tile-main').html(box);
}

function carga_perfil_adm(e) {
  localStorage.removeItem('edit_id');
  $('#perfil-content-title-main').html('Administración');
  $('#perfil-content-subtitle-main').html('Configura tus datos');
  var box = "";
  box += '<div id="perfil-admin-info" class="column is-12 pa-no">';
  box += '</div>';
  box += '<div class="column is-12 pa-one"></div>';
  $('#perfil-content-tile-main').html(box);
  $('#perfil-admin-info').load('templates/perfil/perfil-edit.html');
}

function carga_perfil_plus(e) {
  localStorage.removeItem('edit_id');
  $('#perfil-content-title-main').html('Planes Plus');
  $('#perfil-content-subtitle-main').html('Mejora tus opciones de publicacion y la proyección de tus publicaciones');
  var box = "";
  box += '<div id="perfil-plan-plus" class="column is-12 pa-no">';
  box += '</div>';
  box += '<div class="column is-12 pa-one"></div>';
  $('#perfil-content-tile-main').html(box);
  $('#perfil-plan-plus').load('templates/planes/planes.html');
}

function carga_perfil_pub(e) {
  //LEE DESTINO
  var src = e;
  var stringaUth_key = localStorage.getItem("aUth_key");
  var stringaUth_user = localStorage.getItem("aUth_user");
  //loader
  loader($('#perfil-content-tile-main'));

  //REVISA SRC Y CAMBIA DESTINATARIOS
  switch(src) {
    case 'pub-man':
      // code block
      pub_man();
      break
    case 'pub-fil':
      // code block
      pub_fil();
      break
    case 'pub-not':
      // code block
      pub_not();
      break
    default:
      toast('Error, 001')
  }
  //PUB MAN MANEJO DE PUBLICACIONES
  function pub_man(e) {
    $('#perfil-content-box').html('');
    localStorage.removeItem('edit_id');
    toast('Publicaciones');
    $.ajax({
      type: "POST",
      url: "https://searchtwome.com/data/php/products/products-user.php",
      data: {
        auth : stringaUth_key,
        user : stringaUth_user
      },
      async:true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        $('#perfil-content-title-main').html('Manejador de Publicaciones');
        $('#perfil-content-subtitle-main').html(data.length + ' publicaciones <i class="fas fa-plus-circle has-text-link handed publish-add"></i>');
        $('#perfil-content-tile-main').html(plantilla_pub());
        var box = "";
        $.each(data, function (name, value) {
          if (value.success) {
            var pro_index = value.pro_index;
            box += '<a class="panel-block handed publish-show" id=' + pro_index + '>';
              box += '<span class="panel-icon">';
                box += '<i class="fas fa-home" aria-hidden="true"></i>';
              box += '</span>';
              box += value.nom + '<span class="has-text-grey-light fo-w-l">&nbsp;id:&nbsp;' + value.pro_index + '</span>';
            box += '</a>';
          }
        });
        box += '<div class="column is-12 pa-one"></div>';
        $('#panel-content').html(box)

        $('.publish-show').on('click', function(e){
          var pro_index = $(this).attr('id');
          toast(pro_index);
          $('#panel-content').toggle('slow');
          //ADD BOX WITH CONTENT
          localStorage.setItem('edit_id',pro_index);
          $('#perfil-content-box').load('templates/products/products-add.html')
        })

        $('.reset-toggle').on('click', function(e) {
          $('#perfil-content-box').html('');
          $('#panel-content').toggle('slow')
        })
        //PRODUCT ADD
        $('.publish-add').on('click', function(e) {
          $('#panel-content').toggle('slow');
          //ADD BOX WITH CONTENT
          localStorage.removeItem('edit_id');
          $('#perfil-content-box').load('templates/products/products-add.html')
        })
      },
      error: function(xhr, tst, err) {
        console.log(err);
      }
    })
  }

  //PUB FIL MANEJO DE FILTROS
  function pub_fil(e) {
    $('#perfil-content-box').html('');
    localStorage.removeItem('edit_id');
    toast('Filtros');

    var cat_nombre;
    nombre_categorias();

    function nombre_categorias() {
      $.ajax({
        type: "POST",
        url: "https://searchtwome.com/data/php/products/products-categorias.php",
        async:true,
        dataType : 'json',
        crossDomain: true,
        context: document.body,
        cache: false,
        success: function(data) {
          cat_nombre = data;
          carga_filtros()
        },
        error: function(xhr, tst, err) {
          console.log(err)
        }
      })
    }

    function carga_filtros(e) {
      $.ajax({
        type: "POST",
        url: "https://searchtwome.com/data/php/main/main-filter-user.php",
        data: {
          auth : stringaUth_key,
          user : stringaUth_user
        },
        async:true,
        dataType : 'json',
        crossDomain: true,
        context: document.body,
        cache: false,
        success: function(data) {
          $('#perfil-content-title-main').html('Manejo de Filtros');
          $('#perfil-content-tile-main').html(plantilla_pub());
          var box = "", renta, n = 0;
          $.each(data, function (name, value) {
            if (value.success) {
              n++;
              var index = value.index;
              if (value.renta == 1) { renta = "renta" } else { renta = "venta"}
              box += '<a class="panel-block handed filter-show" id=' + index + '>';
                box += '<span class="panel-icon">';
                  box += '<i class="fas fa-filter" aria-hidden="true"></i>';
                box += '</span>';
                box += '<b class="has-background-success has-text-white pa-lr-quo bo-r-five">' + renta + '</b>&nbsp;Categoria:&nbsp;<em class="has-text-grey">' + compara_categorias(value.categorias) + '</em><span class="has-text-grey-light fo-w-l">&nbsp;precio: $' + money_format(value.precio) + '</span>';
              box += '</a>';
            }
          });
          $('#perfil-content-subtitle-main').html(n + ' Filtros');
          $('#panel-content').html(box)

          $('.filter-show').on('click', function(e){
            var index = $(this).attr('id');
            toast(index);
            $('#panel-content').toggle('slow');
            //ADD BOX WITH CONTENT
            localStorage.setItem('edit_id',index);
            $('#perfil-content-box').load('templates/perfil/perfil-filter-add.html')
          })

          $('.reset-toggle').on('click', function(e) {
            $('#perfil-content-box').html('');
            $('#panel-content').toggle('slow')
          })
        },
        error: function(xhr, tst, err) {
          console.log(err);
        }
      })
    }

    function compara_categorias(e) {
      var index = e, nombre;
      $.each(cat_nombre, function(name,value){
        if (index == value.cate_index) {
          nombre = value.nom
        }
      })
      if (nombre == undefined) {
        return "No categoria"
      } else {
        return nombre
      }
    }
  }

  //PUB NOT MANEJO DE NOTIFICACIONES
  function pub_not(e) {
    localStorage.removeItem('edit_id');
    toast('proximamante');
    $('#perfil-content-title-main').html('Notificaciones');
    $('#perfil-content-subtitle-main').html('Proximamente');
    $('#perfil-content-tile-main').html('')
  }

  //PLANTILLA A UTILIZAR
  function plantilla_pub(e) {
    var txt = '<nav class="panel column is-12 pa-no">';
      txt += '<p class="panel-heading has-background-success has-text-white">';
        txt += 'Compendio';
      txt += '</p>';
      txt += '<div class="panel-block">';
        txt += '<p class="control has-icons-left">';
          txt += '<input class="input" type="text" placeholder="Buscar">';
          txt += '<span class="icon is-left">';
            txt += '<i class="fas fa-search" aria-hidden="true"></i>';
          txt += '</span>';
        txt += '</p>';
      txt += '</div>';
      txt += '<div id="panel-content">';
      txt += '</div>';
      txt += '<div class="panel-block">';
        txt += '<button class="button is-link is-outlined is-fullwidth reset-toggle">';
          txt += 'Reset';
        txt += '</button>';
      txt += '</div>';
    txt += '</nav>';
    return txt
  }
}
