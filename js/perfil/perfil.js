$(document).ready(function() {
  //CASCADE LOAD filter--main--nav--content
  carga_navigator();
  carga_footer();
})

function carga_navigator(e) {
  $('#navigator').load('templates/navigator/navigator-main.php');
  //CASCADE LOAD
  carga_perfil_main()
}

function carga_perfil_main(e) {
  $('#perfil-main').load('templates/perfil/perfil-main.html');
  //CASCADE LOAD
  carga_perfil_nav()
}

function carga_perfil_nav(e) {
  $('#perfil-nav').load('templates/perfil/perfil-nav.html');
  //CASCADE LOAD
  carga_perfil_content();
}

function carga_perfil_content(e) {
  $('#perfil-content').load('templates/perfil/perfil-content.html');
}

function carga_footer(e) {
  $('#footer').load('templates/footer/footer.html');
}
