// Avoid `console` errors in browsers that lack a console.
(function() {
  var method;
  var noop = function () {};
  var methods = [
    'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
    'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
    'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
    'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
  ];
  var length = methods.length;
  var console = (window.console = window.console || {});

  while (length--) {
    method = methods[length];

    // Only stub undefined methods.
    if (!console[method]) {
      console[method] = noop;
    }
  }
}());

// Place any jQuery/helper plugins in here.
$(document).ready(function() {
  //CREA UN TOAST MODAL
  window.toast = function(e) {
    var toast = '<span id="toast" class="tag is-dark">' + e + '</span>';
    $('body').append(toast);
    setTimeout(function(){
      if ($('#toast').length > 0) {
        $('#toast').remove();
      }
    }, 5000)
  }
  //CREA UN MODAL PARA TODOS Y TODO
  window.modal = function(incoming) {
    //DESTRUYE VIEJOS MODALES
    if ($('.modal').length) {
      $('.modal').remove();
    }
    //CREA UN MODAL NUEVO
    var content = incoming;
    var modal = '<div class="modal">';
    modal += '<div class="modal-background"></div>';
    modal += '<div class="modal-content">';
    modal += content;
    modal += '</div>';
    modal += '<button class="modal-close is-large" aria-label="close"></button>';
    modal += '</div>';
    $('body').prepend(modal);
    $('.modal').css('z-index','9000');
    $('.modal').toggleClass("is-active");
    $('.modal-close').on('click', function(){
      $('.modal').toggleClass("is-active");
    });
    $('.modal-background').on('click', function(){
      $('.modal').toggleClass("is-active");
    })
  }
  //CIERRAMODAL
  window.xmodal = function(e) {
    //DESTRUYE VIEJOS MODALES
    if ($('.modal').length) {
      $('.modal').remove();
    }
  }
  //TIPO DE CAMBIO
  window.currency_get = function(e) {
    var result;
    //CAMBIA TIPO DE CAMBIO DE ACUERDO A LA API https://api.exchangeratesapi.io/latest?base=MXN&symbols=USD
    $.ajax({
      method: 'get',
      url: 'https://api.exchangeratesapi.io/latest?base=MXN&symbols=USD',
      dataType: "json",
      context: document.body,
      cache: false,
      success: function(data) {
        $.each(data, function (name, value) {
          result = value.USD;
          return false
        });
        localStorage.setItem('currency',result);
      },
      error: function(xhr, tst, err) {
        alert('El tiempo de espera fue superado, por favor intentalo en un momento mas');
        closeall();
      },
      timeout: 10000
    });
  }
  window.money_format = function(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
  //LOADER DE OBJETOS
  window.loader = function(e) {
    var destiny = e;
    var txt = '<div class="loader-wrapper">';
      txt += '<div class="loader is-loading"></div>';
    txt += '</div>';
    destiny.addClass('is-relative');
    destiny.append(txt);
    $('.loader-wrapper').addClass('is-active');
  }
  window.xloader = function(e) {
    $('.loader-weapper').remove()
  }
  //MONEY FORMAT
  window.money_format = function (x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
})
