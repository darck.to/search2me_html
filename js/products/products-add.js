$(document).ready(function() {
  var opc, mult, inum;

  carga_plan();
  carga_products_map();
  carga_categorias();

  function carga_plan(e) {
    var stringaUth_key = localStorage.getItem("aUth_key");
    var stringaUth_user = localStorage.getItem("aUth_user");
    $.ajax({
      type: "POST",
      url: "https://searchtwome.com/data/php/user/user-plan.php",
      data: {
        auth : stringaUth_key,
        user : stringaUth_user
      },
      async:true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        var pro_index;
        var img_edit = 'img/stock.png';
        $('#multimediaCajas').html('');
        $.each(data, function (name, value) {

          mult = value.mult;
          opc = value.opc;
          carga_opciones(opc);
          $('#opcionesNo').html(opc);
          $('#multimediaNo').html(mult);
          for (i = 0; i < mult; i++) {
            imgc = '<div class="column is-3 ma-one pa-one bo-s">';
              imgc += '<div class="columns is-mobile is-multiline is-relative">';
                imgc += '<div class="column is-full product-container he-120">';
                  //SI ES EDICION
                  if (localStorage.getItem("edit_id") != null) {
                    pro_index = localStorage.getItem("edit_id");
                    img_edit = 'https://searchtwome.com/data/assets/pro_img/' + pro_index + '/' + pro_index + '_' + i + '.png';
                  } else {
                    pro_index = null;
                  }
                  imgc += '<img src="' + img_edit + '" id="imgContainer' + i + '" class="product-img-container">';
                imgc += '</div>';
                imgc += '<div class="file is-absolute-b-0px wi-100"><label class="file-label wi-100"><input id="file_' + i + '" class="file-input input-file-uploader" type="file" name="resume" accept="image/*" identifier="' + i + '" idimage="' + i + '" idpublish="' + pro_index + '">';
                imgc += '<span class="file-cta wi-100 bo-r-no bo-no"><span class="file-icon"><i class="fas fa-upload"></i></span><span class="file-label">Choose a file…</span></span></label></div>';
              imgc += '</div>';
            imgc += '</div>';
            $('#multimediaCajas').append(imgc);
          }
          function readURL(input, id) {
            var reader = new FileReader();
            reader.onload = function (e) {
              $('#imgContainer' + id).attr('src', e.target.result)
            }
            if (input.files && input.files[0]) {
              reader.readAsDataURL(input.files[0]);
            }
          }
          $(".input-file-uploader").change(function(){
            var id = $(this).attr('identifier');
            readURL(this, id)
          });
          //REEMPLAZA IMAGEN
          $('.input-file-uploader').change(function(e) {
            //loading
            $(this).addClass('is-loading');
            var id_img = $(this).attr('idimage');
            var id_publish = $(this).attr('idpublish');
            var ruta = "https://searchtwome.com/data/php/products/products-editimgw.php";
            if (id_publish == 'null') {
              ruta = "https://searchtwome.com/data/php/products/products-preimg.php";
            }
            var form_data = new FormData();
            var file = $('#file_' + id_img)[0].files[0];
            form_data.append('auth',stringaUth_key);
            form_data.append('user', stringaUth_user);
            form_data.append('num',id_img);
            form_data.append('id_publish', id_publish);
            form_data.append('file',file);
            $.ajax({
              type: "POST",
              url: ruta,
              data: form_data,
              contentType: false,
              processData: false,
              success: function(data) {
                $.each(data, function (name, value) {
                  if (value.success) {
                    toast(value.message);
                    $(this).removeClass('is-loading')
                  } else {
                    toast(value.error)
                  }
                })
              },
              error: function(xhr, tst, err) {
                toast(err)
              }
            });
          })
          //IMAGENES ROTAS
          $('.product-img-container').on('error', function () {
              $(this).attr('src', 'img/stock.png');
          });
          // Select from gallery
          $(".but_select").click(function(){
            inum = $(this).attr('val');
            navigator.camera.getPicture(onSuccess, onFail, { quality: 90,
              targetWidth: 900,
              targetHeight: 900,
              sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
              allowEdit: true,
              destinationType: Camera.DestinationType.FILE_URI
            });
          });
        });
      },
      error: function(xhr, tst, err) {
        toast(err)
      }
    });

    function carga_opciones(counter) {
      $.ajax({
        type: "POST",
        url: "https://searchtwome.com/data/php/products/products-opc.php",
        data: {
          auth : stringaUth_key,
          user : stringaUth_user
        },
        async:true,
        dataType : 'json',
        crossDomain: true,
        context: document.body,
        cache: false,
        success: function(data) {
          $('#product-opc').html('');
          var opc = "";
          $.each(data, function(id,content){
            opc += '<div class="column is-6 is-relative pa-no">';
              opc += '<div class="filter-options opt-' + content.id + '">';
                opc += '<i class="has-text-white ' + content.icon + '"></i>';
              opc += '</div>';
              if (content.type == "integer") {input = '<input id="num-' + content.id +'" class="input integer-input" type="number" value=1>';} else { input = "";}
              opc += '<span class="is-hidden"><small class="has-text-white-ter">' + content.name + '</small>' + input + '</span>';
            opc += '</div>';
          })
          $('#product-opc').append(opc);
          //OPCIONES
          $('.filter-options').on('click', function(e){
            var count = $('.count').length;
            if (count == counter) {
              if ($(this).hasClass('count')) {
                $(this).toggleClass('has-background-primary count');
                $(this).siblings('span').toggleClass('is-hidden');
                $(this).parent().toggleClass('has-background-primary');
                $(this).parent().toggleClass('shadow-one');
              }
            } else {
              //SELECCIONA LA OPCION
              $(this).toggleClass('has-background-primary count');
              $(this).siblings('span').toggleClass('is-hidden');
              $(this).parent().toggleClass('has-background-primary');
              $(this).parent().toggleClass('shadow-one');
            }
          });
        },
        error: function(xhr, tst, err) {
          console.log(err);
        }
      })
      //SI ES EDICION
      var stringaUth_key = localStorage.getItem("aUth_key");
      var stringaUth_user = localStorage.getItem("aUth_user");
      var pro_index = localStorage.getItem("edit_id");
      $.ajax({
        type: "POST",
        url: "https://searchtwome.com/data/php/products/products-show.php",
        data: {
          auth : stringaUth_key,
          user : stringaUth_user,
          pro_index : pro_index
        },
        async:true,
        dataType : 'json',
        crossDomain: true,
        context: document.body,
        cache: false,
        success: function(data) {
          $.each(data, function(key,value){
            if (value.success == true) {
              if (parseInt(value.cuartos) > 0) {
                $('.opt-cuartos').toggleClass('has-background-primary count');
                $('.opt-cuartos').siblings('span').toggleClass('is-hidden');
                $('.opt-cuartos').parent().toggleClass('has-background-primary');
                $('.opt-cuartos').parent().toggleClass('shadow-one');
                $('#num-cuartos').val(value.cuartos);
              }
              if (value.jacuzzi == 'true') {
                $('.opt-jacuzzi').toggleClass('has-background-primary count');
                $('.opt-jacuzzi').siblings('span').toggleClass('is-hidden');
                $('.opt-jacuzzi').parent().toggleClass('has-background-primary');
                $('.opt-jacuzzi').parent().toggleClass('shadow-one');
              }
              if (value.air == 'true') {
                $('.opt-air').toggleClass('has-background-primary count');
                $('.opt-air').siblings('span').toggleClass('is-hidden');
                $('.opt-air').parent().toggleClass('has-background-primary');
                $('.opt-air').parent().toggleClass('shadow-one');
              }
              if (value.jardin == 'true') {
                $('.opt-jardin').toggleClass('has-background-primary count');
                $('.opt-jardin').siblings('span').toggleClass('is-hidden');
                $('.opt-jardin').parent().toggleClass('has-background-primary');
                $('.opt-jardin').parent().toggleClass('shadow-one');
              }
              if (value.trasero == 'true') {
                $('.opt-trasero').toggleClass('has-background-primary count');
                $('.opt-trasero').siblings('span').toggleClass('is-hidden');
                $('.opt-trasero').parent().toggleClass('has-background-primary');
                $('.opt-trasero').parent().toggleClass('shadow-one');
              }
              if (value.chimenea == 'true') {
                $('.opt-chimenea').toggleClass('has-background-primary count');
                $('.opt-chimenea').siblings('span').toggleClass('is-hidden');
                $('.opt-chimenea').parent().toggleClass('has-background-primary');
                $('.opt-chimenea').parent().toggleClass('shadow-one');
              }
              if (parseInt(value.banos) > 0) {
                $('.opt-banos').toggleClass('has-background-primary count');
                $('.opt-banos').siblings('span').toggleClass('is-hidden');
                $('.opt-banos').parent().toggleClass('has-background-primary');
                $('.opt-banos').parent().toggleClass('shadow-one');
                $('#num-' + value.key).val(value.banos);
              }
              if (parseInt(value.cochera) > 0) {
                $('.opt-chochera').toggleClass('has-background-primary count');
                $('.opt-chochera').siblings('span').toggleClass('is-hidden');
                $('.opt-chochera').parent().toggleClass('has-background-primary');
                $('.opt-chochera').parent().toggleClass('shadow-one');
                $('#num-cochera').val(value.cochera);
              }
              if (value.piscina == 'true') {
                $('.opt-piscina').toggleClass('has-background-primary count');
                $('.opt-piscina').siblings('span').toggleClass('is-hidden');
                $('.opt-piscina').parent().toggleClass('has-background-primary');
                $('.opt-piscina').parent().toggleClass('shadow-one');
              }
              if (value.terraza == 'true') {
                $('.opt-terraza').toggleClass('has-background-primary count');
                $('.opt-terraza').siblings('span').toggleClass('is-hidden');
                $('.opt-terraza').parent().toggleClass('has-background-primary');
                $('.opt-terraza').parent().toggleClass('shadow-one');
              }
              if (value.balcon == 'true') {
                $('.opt-balcon').toggleClass('has-background-primary count');
                $('.opt-balcon').siblings('span').toggleClass('is-hidden');
                $('.opt-balcon').parent().toggleClass('has-background-primary');
                $('.opt-balcon').parent().toggleClass('shadow-one');
              }
              if (value.seguridad == 'true') {
                $('.opt-seguridad').toggleClass('has-background-primary count');
                $('.opt-seguridad').siblings('span').toggleClass('is-hidden');
                $('.opt-seguridad').parent().toggleClass('has-background-primary');
                $('.opt-seguridad').parent().toggleClass('shadow-one');
              }
              if (value.recepcion == 'true') {
                $('.opt-recepcion').toggleClass('has-background-primary count');
                $('.opt-recepcion').siblings('span').toggleClass('is-hidden');
                $('.opt-recepcion').parent().toggleClass('has-background-primary');
                $('.opt-recepcion').parent().toggleClass('shadow-one');
              }
              if (value.gimnasio == 'true') {
                $('.opt-gimnasio').toggleClass('has-background-primary count');
                $('.opt-gimnasio').siblings('span').toggleClass('is-hidden');
                $('.opt-gimnasio').parent().toggleClass('has-background-primary');
                $('.opt-gimnasio').parent().toggleClass('shadow-one');
              }
              $('#i-tit').val(value.nom);
              $('#i-lat').val(value.lat);
              $('#i-lng').val(value.lng);
              $('#i-cal').val(value.cal);
              $('#i-num').val(value.num);
              $('#i-col').val(value.col);
              $('#i-ciu').val(value.ciu);
              $('#i-cp').val(value.cp);
              $('#i-est').val(value.est);
              $('#i-pai').val(value.pai);
              $('#i-des').html(value.des);
              if(value.ren == 1) {$('#i-ren').prop( "checked", true );} else {$('#i-ven').prop( "checked", true );}
              $('#i-pre').val(value.pre);
              $('#i-cat').prepend('<option value=' + value.cate_index + ' selected>' + value.cat + '</option>');
              //TEXTO DE EDICION IN header
              $('#editTxt').html(translate_string('products_edit_press'));
              //BOTON DE ELIMINAR
              $('.publish-button').html('Editar');
              var del = '<div class="columns is-12 has-text-centered pa-one width-100">';
                del += '<button class="button has-background-danger has-text-white input-button-rounded input-button-small delete-button" onclick=elimina_producto("' + pro_index + '")>' + translate_string('products_edit_del') + '</button>'
              del += '</div>';
              $('#delButton').html(del);
            }
          })
        },
        error: function(xhr, tst, err) {
          toast(err);
        }
      });
    }

  }

  function carga_categorias(e) {
    //CURRENCY
    $('.product-symbol').html(localStorage.getItem('currency_symbol'));
    $('#categorias-select').toggleClass('is-loading');
    $.ajax({
      type: "POST",
      url: "https://searchtwome.com/data/php/products/products-categorias.php",
      async:true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        $('#i-cat').html('');
        $.each(data, function (name, value) {
          $('#i-cat').append('<option value=' + value.cate_index + '>' + value.nom + '</option>');
        });
        $('#i-cat').toggleClass('is-loading');
      },
      error: function(xhr, tst, err) {
        alert(translate_string('error_ajax'));
        closeall();
      }
    })
  }

  translate();

})

function carga_products_map(e) {

  var promap = L.map('productsmap').setZoom(13);

  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: ''
  }).addTo(promap);

  function onLocationFoundProduct(e) {

    var geocodeService = L.esri.Geocoding.geocodeService();
    var marker;

    promap.on('click', function (e) {
      geocodeService.reverse().latlng(e.latlng).run(function (error, result) {
        if (error) {
          return;
        }
        if (marker) { // check
            promap.removeLayer(marker); // remove
        }
        marker = L.marker(result.latlng).addTo(promap).bindPopup(result.address.Match_addr).openPopup();
        $('#i-cal').val(result.address.Address);
        $('#i-num').val(result.address.AddNum);
        $('#i-col').val(result.address.Neighborhood);
        $('#i-ciu').val(result.address.Subregion);
        $('#i-est').val(result.address.Region);
        $('#i-pai').val(result.address.CountryCode);
        $('#i-cp').val(result.address.Postal);
        $('#i-lat').val(e.latlng.lat);
        $('#i-lng').val(e.latlng.lng);
      });
    });

  }

  function onLocationErrorProduct(e) {
    alert(e.message);
  }

  promap.on('locationfound', onLocationFoundProduct);
  promap.on('locationerror', onLocationErrorProduct);

  promap.locate({setView: true, maxZoom: 16});
}

$(".integer-input").on("keypress keyup blur",function (event) {
   $(this).val($(this).val().replace(/[^\d].+/, ""));
    if ((event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }
});

function guarda_producto_bdd(e) {
  var mod = false;
  var list = '<div class="has-background-white ma-half pa-one bo-r-five">';
  list += '<h1 class="fo-w-l has-text-centered has-text-danger">Campos vacios:</h1>';
  list += '<ul>';

  var tit = $('#i-tit').val();
    if (!tit) {list += '<li>Titulo</li>'; mod = true};
  var lat = $('#i-lat').val();
    if (!lat) {list += '<li>Latitud</li>'; mod = true};
  var lng = $('#i-lng').val();
    if (!lng) {list += '<li>Longitud</li>'; mod = true};
  var cal = $('#i-cal').val();
    if (!cal) {list += '<li>Calle</li>'; mod = true};
  var num = $('#i-num').val();
    if (!num) {list += '<li>Numero</li>'; mod = true};
  var col = $('#i-col').val();
    if (!col) {list += '<li>Colonia/Comunidad</li>'; mod = true};
  var ciu = $('#i-ciu').val();
    if (!ciu) {list += '<li>Ciudad/Region</li>'; mod = true};
  var est = $('#i-est').val();
    if (!est) {list += '<li>Estado/Provincia</li>'; mod = true};
  var pai = $('#i-pai').val();
    if (!pai) {list += '<li>Pais</li>'; mod = true};
  var cp = $('#i-cp').val();
    if (!cp) {list += '<li>CP</li>'; mod = true};
  var des = $('#i-des').val();
    if (!des) {list += '<li>Descripcion</li>'; mod = true};
  var ren = $('input[name="ren"]:checked').val();
    if (!ren) {list += '<li>Renta o Venta</li>'; mod = true};
  var pre = $('#i-pre').val();
    if (!pre) {list += '<li>Precio</li>'; mod = true};
  var cat = $('#i-cat').val();
    if (!cat) {list += '<li>Categorias</li>'; mod = true};

  if ($('.count').length == 0) {list += '<li>No hay opciones seleccionadas</li>'; mod = true};

  if ($('.opt-cuartos').hasClass('count')) { var cuartos = $('#num-cuartos').val(); } else { var cuartos = 0; }
  if ($('.opt-jacuzzi').hasClass('count')) { var jacuzzi = true; } else { var jacuzzi = false; }
  if ($('.opt-air').hasClass('count')) { var air = true; } else { var air = false; }
  if ($('.opt-jardin').hasClass('count')) { var jardin = true; } else { var jardin = false; }
  if ($('.opt-trasero').hasClass('count')) { var trasero = true; } else { var trasero = false; }
  if ($('.opt-chimenea').hasClass('count')) { var chimenea = true; } else { var chimenea = false; }
  if ($('.opt-banos').hasClass('count')) { var banos = $('#num-banos').val(); } else { var banos = 0; }
  if ($('.opt-cochera').hasClass('count')) { var cochera = $('#num-cochera').val(); } else { var cochera = 0; }
  if ($('.opt-piscina').hasClass('count')) { var piscina = true; } else { var piscina = false; }
  if ($('.opt-terraza').hasClass('count')) { var terraza = true; } else { var terraza = false; }
  if ($('.opt-balcon').hasClass('count')) { var balcon = true; } else { var balcon = false; }
  if ($('.opt-seguridad').hasClass('count')) { var seguridad = true; } else { var seguridad = false; }
  if ($('.opt-recepcion').hasClass('count')) { var recepcion = true; } else { var recepcion = false; }
  if ($('.opt-gimnasio').hasClass('count')) { var gimnasio = true; } else { var gimnasio = false; }

  list += '</ul>';
  list += '</div>';

  if (mod) {
    modal( );
    $('.modal-content').html(list);
    return false;
  }

  var stringaUth_key = localStorage.getItem("aUth_key");
  var stringaUth_user = localStorage.getItem("aUth_user");
  var cur = localStorage.getItem('currency_symbol');
  var mult = $('#multimediaNo').html();
  var ruta = "https://searchtwome.com/data/php/products/products-add.php";
  var pro_index = null;
  //SI ES EDICION
  if (localStorage.getItem("edit_id") != null) {
    ruta = "https://searchtwome.com/data/php/products/products-edit.php";
    pro_index = localStorage.getItem("edit_id");
  }
  $.ajax({
    type: "POST",
    url: ruta,
    data: {
      auth : stringaUth_key,
      user : stringaUth_user,
      pro_index: pro_index,
      tit : tit,
      lat : lat,
      lng : lng,
      cal : cal,
      num : num,
      col : col,
      ciu : ciu,
      est : est,
      pai : pai,
      cp : cp,
      des : des,
      ren : ren,
      cur : cur,
      pre : pre,
      cat : cat,
      cuartos : cuartos,
      jacuzzi : jacuzzi,
      air : air,
      jardin : jardin,
      trasero : trasero,
      chimenea : chimenea,
      banos : banos,
      cochera : cochera,
      piscina : piscina,
      terraza : terraza,
      balcon : balcon,
      seguridad : seguridad,
      recepcion : recepcion,
      gimnasio : gimnasio,
      mult : mult
    },
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      var list = '<div class="has-background-white ma-half pa-one bo-r-five">';
      $.each(data, function (name, value) {
        if(value.success) {
          //ELIMINAMOS localStorage DE EDICION
          localStorage.removeItem('edit_id');
          list += '<h1 class="has-text-centered ma-one is-size-1 fo-w-l">' + value.info + '</h1>';
          list += '<h1 class="has-text-centered pa-one"><i class="has-text-success far fa-4x fa-smile"></i></h1>';
          $('#perfil-content-box').html('');
          $('#panel-content').toggle('slow')
        } else {
          list += '<h1 class="has-text-centered ma-one is-size-1 fo-w-l">' + value.error + '</h1>';
          list += '<h1 class="has-text-centered pa-one"><i class="has-text-danger far fa-4x fa-frown-open"></i></h1>';
        }
      });
      list += '</div>';
      modal();
      $('.modal-content').html(list);

    },
    error: function(xhr, tst, err) {
      toast(err)
    }
  })
}

function elimina_producto(e) {
  var confirmation = confirm(translate_string('products_confirm_del'));
  if (confirmation) {
    var stringaUth_key = localStorage.getItem("aUth_key");
    var stringaUth_user = localStorage.getItem("aUth_user");
    var pro_index = e;
    $.ajax({
      type: "POST",
      url: "https://searchtwome.com/data/php/products/products-del.php",
      data: {
        auth : stringaUth_key,
        user : stringaUth_user,
        pro_index : pro_index
      },
      async:true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        var list = '<div class="has-background-white ma-half pa-one bo-r-five">';
        $.each(data, function (name, value) {
          if(value.success) {
            list += '<h1 class="has-text-centered ma-one is-size-1 fo-w-l">' + value.info + '</h1>';
            list += '<h1 class="has-text-centered pa-one"><i class="has-text-success far fa-4x fa-smile"></i></h1>';
          } else {
            list += '<h1 class="has-text-centered ma-one is-size-1 fo-w-l">' + value.error + '</h1>';
            list += '<h1 class="has-text-centered pa-one"><i class="has-text-danger far fa-4x fa-frown-open"></i></h1>';
          }
        });
        list += '</div>';
        modal();
        $('.modal-content').html(list)
      },
      error: function(xhr, tst, err) {
        toast(err)
      }
    })
  }
}
