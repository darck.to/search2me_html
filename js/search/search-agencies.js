$(document).ready(function(){
  //CASCADE LOADING search loader --- opciones
  carga_agencies();
})

function carga_agencies(e) {
  $.ajax({
    type: "POST",
    url: "https://searchtwome.com/data/php/products/products-agencies.php",
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      var text = "";
      $.each(data, function(index, content){
        if (content.success) {
          text += '<div class="column is-3">';
          text += '<a href="buscar.php?renta=3&perf_index=' + content.perf_index + '">';
              text += '<div class="box bo-r-no pa-no agencies-box">';
                text += '<img src="' + content.img + '">';
                text += '<h5 class="pa-half fo-w-l">' + content.nom + '</h5>';
              text += '</div>';
            text += '</a>';
          text += '</div>';
        }
        $('#agencies-feed').html(text);
      })
    },
    error: function(xhr, tst, err) {
      console.log(err);
    }
  })
}
