$(document).ready(function(){
  //CASCADE LOADING search loader --- opciones
  carga_categories();
})

function carga_categories(e) {
  $.ajax({
    type: "POST",
    url: "https://searchtwome.com/data/php/products/products-categorias.php",
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      var text = "";
      $.each(data, function(index, content){
        if (content.success) {
          text += '<div class="column is-3">';
            text += '<a href="buscar.php?renta=3&cate_index=' + content.cate_index + '">';
              text += '<div class="box bo-r-no pa-no categorie-box">';
                text += '<img src="' + content.img + '">';
                text += '<h5 class="pa-half fo-w-l">' + content.nom + '</h5>';
              text += '</div>';
            text += '</a>';
          text += '</div>';
        }
        $('#categories-feed').html(text);
      })
    },
    error: function(xhr, tst, err) {
      console.log(err);
    }
  })
}
