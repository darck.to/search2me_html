$(document).ready(function(){
  //CASCADE LOADING search loader --- opciones
  carga_search_slider();
})
//BUSCAR MAIN
$('.buscar-main').on('click', function(e) {
  e.preventDefault();

  //RECIBIMOS VARIABLES DE BUSQUEDA
  var input = $('.search-inputs');
  var inputs = [];
  $.each($("input[name='ren']:checked"), function(){
    inputs.push($(this).val());
  })
  input.each(function(){
    inputs.push(this.value);
  });
  console.log(inputs)

  //BUSCAR CADENAS
  var url = 'buscar.php';
  var form = $('<form action="' + url + '" method="GET" class="is-hidden">' +
  '<input type="text" name="tipo" value=0 />' +
  '<input type="text" name="renta" value="' + inputs[0] + '" />' +
  '<input type="text" name="cate_index" value="' + inputs[1] + '" />' +
  '<input type="text" name="pai" value="' + inputs[2] + '" />' +
  '<input type="text" name="ciu" value="' + inputs[3] + '" />' +
  '<input type="text" name="pri" value="' + inputs[4] + '" />' +
  '</form>');
  $('body').append(form);
  form.submit();
})

function carga_search_slider(e) {
  var slider =  document.getElementById('slider1');
  var output = document.getElementById('number1');
  output.innerHTML = money_format(slider.value);

  slider.oninput = function() {
    output.innerHTML = money_format(this.value);
  };
  //CASCADA LOADING opciones
  carga_search_cat();
}

function carga_search_cat(e) {
  $.ajax({
    type: "POST",
    url: "https://searchtwome.com/data/php/products/products-categorias.php",
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      $('#select-cate').html('');
      $('#select-cate').append('<option selected disabled value=0>Categor&iacute;as</option>');
      $.each(data, function(index, content){
        if (content.success) {
          $('#select-cate').append('<option value="' + content.cate_index + '">' + content.nom + '</option>');
        }
      })
    },
    error: function(xhr, tst, err) {
      console.log(err);
    }
  });
  carga_search_pais()
}

function carga_search_pais(e) {
  $.ajax({
    type: "POST",
    url: "https://searchtwome.com/data/php/products/products-paises.php",
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      $('#select-pais').html('');
      $('#select-pais').append('<option selected disabled value=0>Pa&iacute;ses</option>');
      $.each(data, function(index, content){
        if (content.success) {
          $('#select-pais').append('<option value="' + content.pai + '">' + content.pai + '&nbsp<b class="hast-text-grey">(' + content.num + ')</option>');
        }
      });
      $('#select-pais').on('change',function(){
        var pai = $(this).val();
        carga_search_ciud(pai);
      })
    },
    error: function(xhr, tst, err) {
      console.log(err);
    }
  });
  carga_search_opc();
}

function carga_search_ciud(e) {
  var pai = e;
  $.ajax({
    type: "POST",
    url: "https://searchtwome.com/data/php/products/products-ciudades.php",
    data: {
      pai : pai
    },
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      $('#select-ciud').html('');
      $('#select-ciud').append('<option selected disabled value=0>Ciudades</option>');
      $.each(data, function (index, content) {
        if (content.success) {
          $('#select-ciud').append('<option value=' + content.ciu + '>' + content.ciu + '&nbsp<b class="hast-text-grey">(' + content.num + ')</b></option>')
        }
      })
    },
    error: function(xhr, tst, err) {
      console.log(err);
    }
  })
}

function carga_search_opc(e) {
  $.ajax({
    type: "POST",
    url: "https://searchtwome.com/data/php/products/products-opc.php",
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      $('#search-opc').html('');
      $('#select-opc').append('<option selected disabled value=0>Opciones</option>');
      $.each(data, function(id,content){
        $('#search-opc').append('<option value=' + content.id + '>' + content.name + '</option>')
      })
    },
    error: function(xhr, tst, err) {
      console.log(err);
    }
  })
}
