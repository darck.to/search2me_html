$(document).ready(function(){
  //CASCADE LOADING search loader --- opciones
  carga_masonry();
})

function carga_masonry(e) {
  $.ajax({
    type: "POST",
    url: "https://searchtwome.com/data/php/search/search-plus.php",
    async:true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      var renta, txt;
      $.each(data, function(index, content){
        if (content.success) {
          txt = '';
          if (content.ren == 1) { renta = "renta"} else {renta = "venta"}
          $('#search-mas-' + index).children('img').attr('src','https://searchtwome.com/data/assets/pro_img/' + content.pro_index + '/' + content.pro_index + '_0.png');
          txt += '<a href="muestra.php?pro=' + content.pro_index + '" target="_blank">';
            txt += '<span class="has-text-white search-mas-span te-shadow-grey">';
              txt += '<span class="has-background-link is-size-7 pa-half bo-r-five shadow-grey">' + money_format(content.pre) + '&nbsp;' + content.cur + '</span>';
              txt += '<h3 class="fo-w-b pa-t-5p">' + content.nom + '</h3>';
              txt += '<p class="fo-s-12 no-block">' + content.col + ', ' + content.ciu + ', ' + content.est + '</p>';
              txt += '<p class="fo-s-12 no-block">' + content.pai + '</p>';
              txt += '<p class="no-block pa-t-5p">';
                txt += '<span class="has-background-success is-size-7 ma-r-5p pa-half bo-r-five shadow-grey">' + renta + '</span>';
                txt += '<span class="has-background-link is-size-7 pa-half bo-r-five shadow-grey">' + content.cat + '</span>';
              txt += '</p>';
              txt += '</span>';
          txt += '</a>';
          $('#search-mas-' + index).append(txt);

        }
      })
    },
    error: function(xhr, tst, err) {
      console.log(err);
    }
  })
}
