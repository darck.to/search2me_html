<?php
  session_start();
?>
<!doctype html>
<html class="no-js has-navbar-fixed-top" lang="">

<head>
  <meta charset="utf-8">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="manifest" href="site.webmanifest">
  <link rel="apple-touch-icon" href="icon.png">
  <!-- Place favicon.ico in the root directory -->

  <link rel="stylesheet" href="css/bulma.css">
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/main.css">
  <link type="text/css" rel="stylesheet" href="css/all.min.css"  media="screen,projection"/>
  <link rel="stylesheet" type="text/css" href="css/leaflet.css">
  <link rel="stylesheet" href="css/esri-leaflet-geocoder.css">
  <link rel="stylesheet" href="css/leaflet-routing-machine.css" />

  <meta name="theme-color" content="#fafafa">
  <title>search2me Publicacion</title>
</head>

<body>
  <!--[if IE]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
  <![endif]-->

  <!--Search Navigator-->
  <nav id="navigator" class="navbar is-fixed-top has-shadow" role="navigation" aria-label="main navigation">
  </nav>
  <?php
    if (!empty($_GET['pro'])) {

  ?>
  <?php
    } else {

    }
  ?>

  <section class="section has-background-light">

    <div class="columns">
      <div class="column is-8 is-offset-2">

        <h1 class="has-text-left fo-w-l ma-no-t">Publicacion</h1>
        <div class="container pa-one ma-one">
          <div class="columns is-multiline">
            <div class="column is-8">

              <div class="columns is-multiline">
                <div class="column is-12">
                  <div class="box">
                    <div class="field">
                      <div class="control">
                        <h1 id="p-tit" class="is-size-3 fo-w-l"></h1>
                        <span id="p-cat" class="tag is-info"></span>
                        <span id="p-ren" class="tag is-success"></span>
                      </div>
                    </div>
                    <div class="columns">
                      <div class="column is-12">
                        <h1  class="is-size-5 has-text-grey-light fo-w-b" data-translate="products_h1_gal">Galería</h1>
                        <div id="imgHolder" class="content is-12 has-text-centered">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="column is-12">
                  <div class="box">
                    <div class="content">
                      <h1  class="is-size-5 has-text-grey-light" data-translate="products_h1_due">Propietario</h1>
                      <div id="img-usu" class="handed"></div>
                      <p><span id="p-usu" class="title is-4"></span>&nbsp;<span id="p-mai" class="is-size-6 has-text-grey"></span></p>
                      <p class="subtitle is-6">
                        <span id="p-tel" class="is-size-6 has-text-grey"></span><br>
                        <span id="p-cel" class="is-size-6 has-text-grey"></span><br>
                      </p>
                      <p class="subtitle">
                        <h1  class="is-size-5 has-text-grey-light" data-translate="products_h1_dir">Direccion</h1>
                        <span id="p-cal"></span><br>
                        <span id="p-col"></span><br>
                        <span id="p-ciu"></span><br>
                        <span id="p-pai"></span><br>
                      </p>
                      <p class="subtitle">
                        <h1  class="is-size-5 has-text-grey-light" data-translate="products_h1_des">Descripcion</h1>
                        <p id="p-des"></p>
                      </p>
                      <p class="subtitle">
                        <h1  class="is-size-5 has-text-grey-light" data-translate="products_h1_opc">Opciones</h1>
                        <p id="p-opc"></p>
                      </p>
                      <p class="subtitle">
                        <h1  class="is-size-5 has-text-grey-light" data-translate="products_h1_pre">Precio</h1>
                        <p id="p-pre"></p>
                      </p>
                    </div>
                  </div>
                </div>
              </div>

            </div>
            <div class="column is-4 pa-one">

              <div class="columns is-multiline has-background-white bo-r-five">
                <div class="column is-12">
                  <h1  class="is-size-5 has-text-grey-light fo-w-b">Mapa</h1>
                  <div id="productsmap" class="products-map has-background-white-ter"></div>
                  <small class="is-size-7 is-pulled-right has-text-grey-light" data-translate="products_small_pre">Presiona el marcador para conocer como llegar</small>
                </div>
                <div class="column is-12">
                  <h1  class="is-size-5 has-text-grey-light fo-w-b">Opciones</h1>
                  <div class="columns is-multiline">
                    <div id="product-opc" class="column has-text-centered">
                      <div id="p-cua" class="filter-options">
                        <i class="has-text-white fas fa-cube"></i>
                      </div>
                      <div id="p-jac" class="filter-options">
                        <i class="has-text-white fas fa-hot-tub"></i>
                      </div>
                      <div id="p-air" class="filter-options">
                        <i class="has-text-white fas fa-wind"></i>
                      </div>
                      <div id="p-jar" class="filter-options">
                        <i class="has-text-white fas fa-tree"></i>
                      </div>
                      <div id="p-tra" class="filter-options">
                        <i class="has-text-white fas fa-tree"></i>
                      </div>
                      <div id="p-chi" class="filter-options">
                        <i class="has-text-white fas fa-fire-alt"></i>
                      </div>
                      <div id="p-ban" class="filter-options">
                        <i class="has-text-white fas fa-bath"></i>
                      </div>
                      <div id="p-coc" class="filter-options">
                        <i class="has-text-white fas fa-car-side"></i>
                      </div>
                      <div id="p-pis" class="filter-options">
                        <i class="has-text-white fas fa-swimmer"></i>
                      </div>
                      <div id="p-ter" class="filter-options">
                        <i class="has-text-white fab fa-microsoft"></i>
                      </div>
                      <div id="p-bal" class="filter-options">
                        <i class="has-text-white fas fa-person-booth"></i>
                      </div>
                      <div id="p-seg" class="filter-options">
                        <i class="has-text-white fas fa-user-shield"></i>
                      </div>
                      <div id="p-rec" class="filter-options">
                        <i class="has-text-white fas fa-concierge-bell"></i>
                      </div>
                      <div id="p-gym" class="filter-options">
                        <i class="has-text-white fas fa-dumbbell"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>

      </div>
    </div>

  </section>

  <!--Footer-->
  <div id="footer">
  </div>

  <script src="js/vendor/modernizr-3.8.0.min.js"></script>
  <script type="text/javascript" src="js/leaflet/leaflet.js"></script>
  <script type="text/javascript" src="js/leaflet/esri-leaflet.js"></script>
  <script type="text/javascript" src="js/leaflet/esri-leaflet-geocoder.js"></script>
  <script type="text/javascript" src="js/leaflet/leaflet-routing-machine.js"></script>
  <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.4.1.min.js"><\/script>')</script>
  <script src="js/plugins.js"></script>
  <script src="js/lang.js"></script>
  <script src="js/muestra/muestra.js"></script>
</body>

</html>

<script type="text/javascript">
<?php
  if (!empty($_GET['pro'])) {
    $clave = $_GET['pro'];
    echo "carga_products_map('" . $clave. "');";
  }
?>
function carga_products_map(e) {
  //TIPO DE CAMBIO
  var curr = localStorage.getItem('currency_symbol');
  $('.product-symbol').html(curr);

  var iconNear = L.icon({
    iconUrl: 'img/leaf/near.png',
    iconSize: [35, 35]
  });

  //COORDENADAS PERSONALES
  navigator.geolocation.getCurrentPosition(function(location) {
    var Nlatlng = new L.LatLng(location.coords.latitude, location.coords.longitude);

    var stringaUth_key = localStorage.getItem("aUth_key");
    var stringaUth_user = localStorage.getItem("aUth_user");
    var pro_index = e;
    $.ajax({
      type: "POST",
      url: "https://searchtwome.com/data/php/products/products-show.php",
      data: {
        auth : stringaUth_key,
        user : stringaUth_user,
        pro_index : pro_index
      },
      async:true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        $.each(data, function(key,value){
          if (value.success == true) {
            var promap = L.map('productsmap').setView([value.lat, value.lng], 13);
            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
              attribution: ''
            }).addTo(promap);
            L.marker([value.lat, value.lng], {icon: iconNear}).addTo(promap).on('click', function(e) {
              L.Routing.control({
                waypoints: [
                  L.latLng(e.latlng),
                  L.latLng(Nlatlng)
                ]
              }).addTo(promap);
            });

            //OPCIONES DESCRIPCION Y FILTERS
            var ulopt = '<ul class="is-size-6 has-text-grey">';
              var listart = '<li>';
              var liend = '</li>' ;
              if (parseInt(value.cuartos) > 0) { $('#p-cua').addClass('has-background-grey-dark'); ulopt += listart + translate_string('products_opc_cua') + ':&nbsp;' + value.cuartos + liend; }
              if (value.jacuzzi == 'true') { $('#p-jac').addClass('has-background-grey-dark'); ulopt += listart + translate_string('products_opc_jac') + ':&nbsp;' + translate_string('products_opc_yes') + liend; }
              if (value.air == 'true') { $('#p-air').addClass('has-background-grey-dark'); ulopt += listart + translate_string('products_opc_air') + ':&nbsp;' + translate_string('products_opc_yes') + liend; }
              if (value.jardin == 'true') { $('#p-jar').addClass('has-background-grey-dark'); ulopt += listart + translate_string('products_opc_jar') + ':&nbsp;' + translate_string('products_opc_yes') + liend; }
              if (value.trasero == 'true') { $('#p-tra').addClass('has-background-grey-dark'); ulopt += listart + translate_string('products_opc_tra') + ':&nbsp;' + translate_string('products_opc_yes') + liend; }
              if (value.chimenea == 'true') { $('#p-chi').addClass('has-background-grey-dark'); ulopt += listart + translate_string('products_opc_chi') + ':&nbsp;' + translate_string('products_opc_yes') + liend; }
              if (parseInt(value.banos) > 0) { $('#p-ban').addClass('has-background-grey-dark'); ulopt += listart + translate_string('products_opc_ban') + ':&nbsp;' + value.banos + liend; }
              if (value.cochera > 1) { var txt = translate_string('products_opc_ves'); } else { var txt = translate_string('products_opc_veh');}
              if (parseInt(value.cochera) > 0) { $('#p-coc').addClass('has-background-grey-dark'); ulopt += listart + translate_string('products_opc_coc') + ':&nbsp;' + value.cochera + '&nbsp;' + txt + liend; }
              if (value.piscina == 'true') { $('#p-pis').addClass('has-background-grey-dark'); ulopt += listart + translate_string('products_opc_pis') + ':&nbsp;' + translate_string('products_opc_yes') + liend; }
              if (value.terraza == 'true') { $('#p-ter').addClass('has-background-grey-dark'); ulopt += listart + translate_string('products_opc_ter') + ':&nbsp;' + translate_string('products_opc_yes') + liend; }
              if (value.balcon == 'true') { $('#p-bal').addClass('has-background-grey-dark'); ulopt += listart + translate_string('products_opc_bal') + ':&nbsp;' + translate_string('products_opc_yes') + liend; }
              if (value.seguridad == 'true') { $('#p-seg').addClass('has-background-grey-dark'); ulopt += listart + translate_string('products_opc_seg') + ':&nbsp;' + translate_string('products_opc_yes') + liend; }
              if (value.recepcion == 'true') { $('#p-rec').addClass('has-background-grey-dark'); ulopt += listart + translate_string('products_opc_rec') + ':&nbsp;' + translate_string('products_opc_yes') + liend; }
              if (value.gimnasio == 'true') { $('#p-gim').addClass('has-background-grey-dark'); ulopt += listart + translate_string('products_opc_gim') + ':&nbsp;' + translate_string('products_opc_yes') + liend; }
            ulopt += '</ul>';

            $('#imgHolder').html('');
            for (var i = 0; i < value.multimedia; i++) {
              $('#imgHolder').append('<img class="product-img-showcase handed show-img" src="https://searchtwome.com/data/assets/pro_img/' + pro_index + '/' + pro_index + '_' + i + '.png" />');
            }

            $('.show-img').on('click', function(e){
              modal();
              $('.modal-content').html('');
              for (var i = 0; i < value.multimedia; i++) {
                $('.modal-content').append('<img src="https://searchtwome.com/data/assets/pro_img/' + pro_index + '/' + pro_index + '_' + i + '.png" />');
              }
            })

            var imgusu = 'https://searchtwome.com/data/assets/perf_img/' + value.perf_index + '.png';

            var renta = translate_string('products_input_ven');
            if(value.ren == 1) {renta = translate_string('products_input_ren');}
            $('#p-cat').html(value.cat);
            $('#p-ren').html(renta);
            $('#p-tit').html(value.nom);
            $('#p-tel').html(value.tel);
            $('#p-cel').html(value.cel);
            $('#p-mai').html(value.mai);
            $('#img-usu').html('<img src=' + imgusu + '>');
            $('#p-usu').html(value.usu);
            $('#p-tel').html(value.tel);
            $('#p-cel').html(value.cel);
            $('#p-des').html(value.des);
            $('#p-cal').html(value.cal);
            $('#p-col').html(value.col);
            $('#p-ciu').html(value.ciu + ', cp ' + value.cp);
            $('#p-pai').html(value.est + ', ' + value.pai);
            $('#p-opc').html(ulopt);
            $('#p-des').html(value.des);
            var precio = money_format(value.pre);
            $('#p-pre').html(precio + '<i class="has-background-grey-light is-size-7 has-text-white bo-r-five ma-half pa-half">' + value.cur + '</i>');

            $('.filter-options').on('click', function(e) {
              var who = $(this).children('i');
              $.ajax({
                url: "https://searchtwome.com/data/assets/opc_br/opc_br.json",
                cache: false,
                success: function(results) {
                  $.each(results, function(index,content){
                    if (who.hasClass(content.icon)) {
                      toast(content.name)
                    }
                  })
                },
                error: function(xhr, tst, err) {
                  toast(err);
                }
              })
            })

            $('#img-usu img').on('click', function(e){
              modal();
              $('.modal-content').html('<img src="' + $(this).attr('src') + '" />');
            })
          }
        })
      },
      error: function(xhr, tst, err) {
        toast(err)
      }
    });

  })

}

</script>
