<?php
  session_start();
?>
<!doctype html>
<html class="no-js has-navbar-fixed-top" lang="">

<head>
  <meta charset="utf-8">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="manifest" href="site.webmanifest">
  <link rel="apple-touch-icon" href="icon.png">
  <!-- Place favicon.ico in the root directory -->

  <link rel="stylesheet" href="css/bulma.css">
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/main.css">
  <link type="text/css" rel="stylesheet" href="css/all.min.css"  media="screen,projection"/>
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" />

  <meta name="theme-color" content="#fafafa">
  <title>search2me Mi Perfil</title>
</head>

<body>
  <!--[if IE]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
  <![endif]-->

  <!--Search Navigator-->
  <nav id="navigator" class="navbar is-fixed-top has-shadow" role="navigation" aria-label="main navigation">
  </nav>
  <?php
    if (!isset($_SESSION['auth'])) {
      include 'templates/perfil/perfil-no.html';
    } else {
  ?>
  <!--Filtro Perfil-->
  <div id="perfil-main">
  </div>

  <div class="columns">
    <div class="column is-8 is-offset-2 ">
      <div id="perfil-nav" class="columns is-multiline">
      </div>
    </div>
  </div>

  <?php
    }
  ?>

  <!--Footer-->
  <div id="footer">
  </div>

  <script type="text/javascript" src="js/leaflet/leaflet.js"></script>
  <script type="text/javascript" src="js/leaflet/esri-leaflet.js"></script>
  <script type="text/javascript" src="js/leaflet/esri-leaflet-geocoder.js"></script>
  <script type="text/javascript" src="js/leaflet/leaflet-routing-machine.js"></script>
  <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.4.1.min.js"><\/script>')</script>
  <script src="js/plugins.js"></script>
  <script src="js/lang.js"></script>
  <script src="js/perfil/perfil.js"></script>

  <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
  <script>
    window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
    ga('create', 'UA-XXXXX-Y', 'auto'); ga('set','transport','beacon'); ga('send', 'pageview')
  </script>
  <script src="https://www.google-analytics.com/analytics.js" async></script>
</body>

</html>
