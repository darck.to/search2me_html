<?php
  session_start();
?>
<!--GOOGLE ID-->
<meta name="google-signin-client_id" content="95562540620-tmk9h595dhqo5c76j4t89mjf3dgkqb9n.apps.googleusercontent.com">
  <div class="container">

    <div class="navbar-brand">
      <a class="navbar-item" href=".">
        <img src="img/logo.png" >
      </a>

      <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
      </a>
    </div>

    <div id="navbarBasicExample" class="navbar-menu">
      <div class="navbar-start">

        <a class="navbar-item" href="index.html">Home</a>
        <a class="navbar-item" href="buscar.php">Buscar</a>
        <div class="navbar-item has-dropdown is-hoverable">
          <a class="navbar-link">Publicaciones en Venta</a>
          <div id="menu-cat-venta" class="navbar-dropdown">
          </div>
        </div>

        <div class="navbar-item has-dropdown is-hoverable">
          <a class="navbar-link">Publicaciones en Renta</a>
          <div id="menu-cat-renta" class="navbar-dropdown">
          </div>
        </div>
        
        <?php
          //print_r($_SESSION);
          if (isset($_SESSION['auth'])) {
            echo "<div class=\"navbar-item has-background-success has-hover-primary\">";
              echo "<a class=\"navbar-item has-text-white has-hover-no add-property-nav\">Agregar Propiedad</a>";
            echo "</div>";
           } else {
          }
        ?>

      </div>
      <div class="navbar-end">
        <div class="navbar-item">
          <?php
            //print_r($_SESSION);
            if (isset($_SESSION['auth'])) {
              echo "<a class=\"button is-small is-success ma-r-half\" href=\"perfil.php\">Tu perfil</a>";
              echo "<a id=\"session-close\" class=\"button is-small is-danger\">Cerrar Sesi&oacute;n</a>";
            } else {
              echo "<a id=\"session-nueva\" class=\"button is-small is-warning ma-r-half\">Inicia Sesión/Registrarse</a>";
            }
          ?>
        </div>
      </div>
    </div>

  </div>

<script src="https://apis.google.com/js/platform.js?onload=onLoad" async defer></script>
<script src="js/navigator/navigator.js"></script>