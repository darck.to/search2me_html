<div class="overlay-filter">

  <div class="overlay-fs has-background-white">
    <div class="header-principal">
      <div class="columns multiline is-mobile ma-t-half ma-lr-half">
        <div class="column is-3">
          <h1 class="has-text-left exitFsOverlay" data-translate="products_h1_can">Cancelar</h1>
        </div>
        <div class="column is-6">
          <h1 class="is-size-4 has-text-centered" data-translate="products_h1_pro">Propiedad</h1>
        </div>
        <div class="column is-3">
          <!--<h1 class="has-text-right" data-translate="products_h1_fav">Favorita</h1>-->
        </div>
      </div>
    </div>
    <div class="columns ma-no pa-no">
      <div id="productsmap" class="products-map has-background-white-ter"></div>
      <small class="is-size-7 ma-no pa-no is-pulled-right has-text-grey-light" data-translate="products_small_pre">Presiona el marcador para conocer como llegar</small>
    </div>
    <div class="columns ma-no pa-no">
      <div class="column is-12 pa-one">
        <div class="field">
          <div class="control">
            <h1 id="p-tit" class="is-size-3 fo-w-l"></h1>
            <span id="p-cat" class="tag is-info pa-lr-half"></span>
            <span id="p-ren" class="tag is-success pa-lr-half"></span>
          </div>
        </div>
      </div>
      <div class="column is-12 ma-no-b pa-no-b">
        <div class="columns ma-no-b pa-no-b">
          <div id="product-opc" class="column has-text-centered">
            <div id="p-cua" class="filter-options">
              <i class="has-text-white fas fa-cube"></i>
            </div>
            <div id="p-jac" class="filter-options">
              <i class="has-text-white fas fa-hot-tub"></i>
            </div>
            <div id="p-air" class="filter-options">
              <i class="has-text-white fas fa-wind"></i>
            </div>
            <div id="p-jar" class="filter-options">
              <i class="has-text-white fas fa-tree"></i>
            </div>
            <div id="p-tra" class="filter-options">
              <i class="has-text-white fas fa-tree"></i>
            </div>
            <div id="p-chi" class="filter-options">
              <i class="has-text-white fas fa-fire-alt"></i>
            </div>
            <div id="p-ban" class="filter-options">
              <i class="has-text-white fas fa-bath"></i>
            </div>
            <div id="p-coc" class="filter-options">
              <i class="has-text-white fas fa-car-side"></i>
            </div>
            <div id="p-pis" class="filter-options">
              <i class="has-text-white fas fa-swimmer"></i>
            </div>
            <div id="p-ter" class="filter-options">
              <i class="has-text-white fab fa-microsoft"></i>
            </div>
            <div id="p-bal" class="filter-options">
              <i class="has-text-white fas fa-person-booth"></i>
            </div>
            <div id="p-seg" class="filter-options">
              <i class="has-text-white fas fa-user-shield"></i>
            </div>
            <div id="p-rec" class="filter-options">
              <i class="has-text-white fas fa-concierge-bell"></i>
            </div>
            <div id="p-gym" class="filter-options">
              <i class="has-text-white fas fa-dumbbell"></i>
            </div>
          </div>
        </div>
      </div>
      <div class="columm is-12 pa-one is-block is-pulled-left">
        <h1  class="is-size-5 has-text-grey-light fo-w-b" data-translate="products_h1_gal">Galería</h1>
        <div id="imgHolder" class="content is-12 has-text-centered pa-t-half">
        </div>
      </div>
      <div class="column is-12 pa-one">
        <div class="content">
          <h1  class="is-size-5 has-text-grey-light" data-translate="products_h1_due">Propietario</h1>
          <div id="img-usu"></div>
          <p><span id="p-usu" class="title is-4"></span>&nbsp;<span id="p-mai" class="is-size-6 has-text-grey"></span></p>
          <p class="subtitle is-6">
            <span id="p-tel" class="is-size-6 has-text-grey"></span><br>
            <span id="p-cel" class="is-size-6 has-text-grey"></span><br>
          </p>
          <p class="subtitle">
            <h1  class="is-size-5 has-text-grey-light" data-translate="products_h1_dir">Direccion</h1>
            <span id="p-cal"></span><br>
            <span id="p-col"></span><br>
            <span id="p-ciu"></span><br>
            <span id="p-pai"></span><br>
          </p>
          <p class="subtitle">
            <h1  class="is-size-5 has-text-grey-light" data-translate="products_h1_des">Descripcion</h1>
            <p id="p-des"></p>
          </p>
          <p class="subtitle">
            <h1  class="is-size-5 has-text-grey-light" data-translate="products_h1_opc">Opciones</h1>
            <p id="p-opc"></p>
          </p>
          <p class="subtitle">
            <h1  class="is-size-5 has-text-grey-light" data-translate="products_h1_pre">Precio</h1>
            <p id="p-pre"></p>
          </p>
        </div>
      </div>
      <div class="column has-text-centered">
        <a id="send-message" class="button has-background-primary has-text-white input-button-rounded" data-translate="products_a_con">Contactar</a>
      </div>
    </div>
  </div>

</div>

<script type="text/javascript" src="js/products/products-show.js"></script>
